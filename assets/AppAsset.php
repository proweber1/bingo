<?php
/**
 * @link http://www.yiiframework.com/
 * @copyright Copyright (c) 2008 Yii Software LLC
 * @license http://www.yiiframework.com/license/
 */

namespace app\assets;

use yii\web\AssetBundle;
use yii\web\View;

/**
 * @author Qiang Xue <qiang.xue@gmail.com>
 * @since 2.0
 */
class AppAsset extends AssetBundle
{
    /**
     * @var string
     */
    public $basePath = '@webroot';
    /**
     * @var string
     */
    public $baseUrl = '@web';
    /**
     * @var array
     */
    public $css = [
        'css/page-load.css',
        'css/site.css',
        'css/fonts.css',
        'css/bingo37.css',
        'css/ticket.css',
        'css/sweetalert.css',
    ];
    /**
     * @var array
     */
    public $js = [
        'https://cdn.socket.io/socket.io-1.4.3.js',
        'js/jqueryPlugins/jquery.browser.min.js',
        'js/jqueryPlugins/jquery.PrintArea.js',
        'js/jqueryPlugins/jquery.mask.min.js',
        'js/jqueryPlugins/sweetalert.min.js',
        'js/pace.min.js',
        'js/bingo37.js',
        'js/bingo37.terminals.js',
        'vendor/bower/Chart.js/Chart.min.js'
    ];
    /**
     * @var array
     */
    public $jsOptions = [
        'position' => View::POS_HEAD,
    ];
    /**
     * @var array
     */
    public $depends = [
        'yii\web\YiiAsset',
        'yii\bootstrap\BootstrapAsset',
        'rmrevin\yii\fontawesome\AssetBundle',
    ];
}
