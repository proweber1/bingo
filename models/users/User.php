<?php

namespace app\models\users;

use app\interfaces\DbStorageItem;
use Yii;
use app\components\behaviors\CustomImageUploadBehavior;
use yii\behaviors\AttributeBehavior;
use yii\db\ActiveRecord;
use yii\web\IdentityInterface;

/**
 * This is the model class for table "{{%users}}".
 *
 * @property integer $id
 * @property string $login
 * @property string $password
 * @property string $fullName
 * @property string $photo
 */
class User extends \yii\db\ActiveRecord implements IdentityInterface, DbStorageItem
{
    const SUPERADMIN_ROLE = 'superadmin';
    const CASHIER_ROLE = 'cashier';
    const PROGRAMMER_ROLE = 'programmer';

    const SCENARIO_CREATE_USER = 'createUser';

    /**
     * @var string
     */
    public $passwordConfirm;

    /**
     * @var string
     */
    private $oldHash;

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%users}}';
    }

    /**
     * After downloading the user saves the old password
     * in a variable, if anything, to recover
     */
    public function afterFind()
    {
        $this->oldHash = $this->password;
    }

    /**
     * @param bool $insert
     * @return bool
     */
    public function beforeSave($insert)
    {
        if (parent::beforeSave($insert)) {
            // TODO: Initially this was done by behavior, but with ActiveRecord evenutally had to do so
            if (empty($this->password)) {
                $this->password = $this->oldHash;
            } else {
                $this->password = Yii::$app->getSecurity()->generatePasswordHash($this->password);
            }

            return true;
        }

        return false;
    }

    /**
     * @return array
     */
    public function behaviors()
    {
        return [
            'uploadBehavior' => [
                'class' => CustomImageUploadBehavior::className(),
                'attribute' => 'photo',
                'thumbs' => [
                    'thumb' => ['width' => 250, 'height' => 250],
                ],
                'filePath' => '@webroot/images/avatars/[[attribute]]',
                'fileUrl' => '@web/images/avatars/[[attribute]]',
                'thumbPath' => '@webroot/images/avatars/thumbs/[[attribute]]',
                'thumbUrl' => '@web/images/avatars/thumbs/[[attribute]]',
            ]
        ];
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['login', 'fullName'], 'required'],
            [['password', 'passwordConfirm'], 'required', 'on' => self::SCENARIO_CREATE_USER],
            [['login'], 'string', 'max' => 20],
            ['photo', 'file', 'extensions' => 'jpg, jpeg, gif, png', 'checkExtensionByMimeType' => false],
            [['password', 'passwordConfirm'], 'string', 'max' => 64],
            ['passwordConfirm', 'compare', 'compareAttribute' => 'password'],
            [['fullName'], 'string', 'max' => 250],
            [['login'], 'unique'],
            ['photo', 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'login' => 'Логин',
            'password' => 'Пароль',
            'passwordConfirm' => 'Повторите пароль',
            'fullName' => 'Полное имя',
            'photo' => 'Фотография',
        ];
    }

    /**
     * @param $password
     * @return bool
     */
    public function validatePassword($password)
    {
        return Yii::$app->security->validatePassword($password, $this->password);
    }

    /**
     * @inheritdoc
     * @return Bingo37UsersQuery the active query used by this AR class.
     */
    public static function find()
    {
        return new Bingo37UsersQuery(get_called_class());
    }

    /**
     * @param int|string $userId
     * @return null|static
     */
    public static function findIdentity($userId)
    {
        return static::findOne($userId);
    }

    /**
     * @param $username
     * @return null|static
     */
    public static function findByUsername($username)
    {
        return static::findOne(['login' => $username]);
    }

    public static function findIdentityByAccessToken($token, $type = null)
    {
        // TODO: Implement findIdentityByAccessToken() method.
    }

    /**
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    public function getAuthKey()
    {
        // TODO: Implement getAuthKey() method.
    }

    public function validateAuthKey($authKey)
    {
        // TODO: Implement validateAuthKey() method.
    }
}
