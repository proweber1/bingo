<?php

namespace app\models;

use Yii;
use app\services\user\UserLoggerFactory;
use app\interfaces\obpattern\ObServer;
use app\interfaces\obpattern\ObSubject;

/**
 * Class LoginLog
 * @package app\models
 */
class LoginLogObServer implements ObServer
{
    const MAIN_ACTION = 'login';

    /**
     * @inheritdoc
     */
    public function update($action, ObSubject $subject, $message = null)
    {
        if ($action === self::MAIN_ACTION) {
            $ul = UserLoggerFactory::create();
            $ul->log();
        }
    }
}