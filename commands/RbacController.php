<?php
/**
 * Created by PhpStorm.
 * User: vjacheslavgusser
 * Date: 11.01.16
 * Time: 17:08
 */

namespace app\commands;

use app\models\users\User;
use Yii;
use yii\console\Controller;

/**
 * Class RbacController
 * @package app\commands
 */
class RbacController extends Controller
{
    /**
     * The creation of permissions and roles
     * based on the rbac
     *
     * @see http://www.yiiframework.com/doc-2.0/guide-security-authorization.html
     */
    public function actionInit()
    {
        $auth = Yii::$app->getAuthManager();

        $createTicket = $auth->createPermission('createTicket');
        $createCashier = $auth->createPermission('addCashierPermission');
        $editSettings = $auth->createPermission('settingsEditPermission');
        $editSystemKeys = $auth->createPermission('editSystemKeys');

        $auth->add($createTicket);
        $auth->add($createCashier);
        $auth->add($editSettings);
        $auth->add($editSystemKeys);

        $cashierRole = $auth->createRole(User::CASHIER_ROLE);
        $cashierRole->description = 'Кассир';

        $auth->add($cashierRole);
        $auth->addChild($cashierRole, $createTicket);

        $superAdmin = $auth->createRole(User::SUPERADMIN_ROLE);
        $superAdmin->description = 'Супер администратор';

        $auth->add($superAdmin);

        $auth->addChild($superAdmin, $editSettings);
        $auth->addChild($superAdmin, $createCashier);
        $auth->addChild($superAdmin, $cashierRole);

        $programmer = $auth->createRole(User::PROGRAMMER_ROLE);
        $programmer->description = 'Программист';

        $auth->add($programmer);
        $auth->addChild($programmer, $editSystemKeys);

        $auth->assign($superAdmin, 1);
        $auth->assign($programmer, 2);
    }
}