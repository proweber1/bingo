<?php

namespace tests\codeception\_pages;

use yii\codeception\BasePage;

/**
 * Class TicketCreatePage
 * @package tests\codeception\_pages
 * @property \AcceptanceTester|\FunctionalTester $actor
 */
class TicketCreatePage extends BasePage
{
    public $route = 'tickets/tickets/create';

    public function createTicket($amount)
    {
        $this->actor->fillField('input[name="TicketCreateForm[amount]"]', $amount);
        $this->actor->click('.create-ticket');
    }
}