<?php

use tests\codeception\_pages\LoginPage;

class LoginFormTest extends \yii\codeception\TestCase
{
    /**
     * @var \FunctionalTester
     */
    protected $tester;
    /**
     * @var LoginPage
     */
    protected $page;

    /**
     * Installation of all necessary variables
     */
    protected function _before()
    {
        $this->page = LoginPage::openBy($this->tester);
    }

    /**
     * Reset all the variables
     */
    protected function _after()
    {
        $this->page = null;
    }

    public function testIsLoginPage()
    {
        $I = $this->tester;

        $I->wantTo('ensure that login works');
        // assert
        $I->see('Войти в систему', 'h3');
    }

    public function testIsEmptyFieldsValidationErrors()
    {
        $this->tester->amGoingTo('try to login with empty credentials');

        $this->page->login('', '');

        $this->tester->expectTo('see validations errors');
        // TODO: two assert, bad!
        $this->tester->see('Необходимо заполнить «Имя пользователя».');
        $this->tester->see('Необходимо заполнить «Пароль».');
    }

    public function testWrongLogin()
    {
        $this->tester->amGoingTo('try to login with wrong credentials');

        $this->page->login('admin', 'admin');

        $this->tester->expectTo('see validations errors');
        // Good
        $this->tester->see('Incorrect username or password.');
    }

    public function testIsLoginSuccessful()
    {
        $this->tester->amGoingTo('try to login with correct credentials');
        $this->page->login('admin', 'XWuiYdlj');

        $this->tester->expectTo('see user info');
        // Cool
        $this->tester->see('Administrator');
    }

}