<?php

use tests\codeception\_pages\TicketCreatePage;
use tests\codeception\_pages\LoginPage;

class CreateTicketFormTest extends \Codeception\TestCase\Test
{
    /**
     * @var \FunctionalTester
     */
    protected $tester;
    /**
     * @var TicketCreatePage
     */
    protected $page;

    protected function _before()
    {
        $loginPage = LoginPage::openBy($this->tester);
        $loginPage->login('admin', 'XWuiYdlj');

        $this->page = TicketCreatePage::openBy($this->tester);
    }

    protected function _after()
    {
        $this->page = null;
    }

    // tests
    public function testIsTicketCreatePage()
    {
        $this->tester->amGoingTo('Ticket create page');

        $this->tester->amOnPage('/tickets/tickets/create');
        $this->tester->see('Создать билет');
    }

    public function testValidationError()
    {
        $this->page->createTicket('');
        $this->tester->see('Необходимо заполнить «Сумма депозита».');
    }

    public function testTicketCreated()
    {
        $this->page->createTicket(9500);

        $this->tester->see(number_format(9500, 2).' kzt', '.ticket-amount');
        $this->tester->see('История билета', 'h3');
    }
}