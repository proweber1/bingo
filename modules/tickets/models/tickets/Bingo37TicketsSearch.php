<?php

namespace app\modules\tickets\models\tickets;

use app\models\users\User;
use app\modules\tickets\models\tickets_history\Bingo37TicketsHistory;
use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use yii\helpers\ArrayHelper;

/**
 * Bingo37TicketsSearch represents the model behind the search form about `app\modules\tickets\models\tickets\Bingo37Tickets`.
 */
class Bingo37TicketsSearch extends Bingo37Tickets
{
    public $issuedMe = true;

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id', 'cashierId', 'created', 'updated', 'status', 'barcode', 'issuedMe'], 'integer'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * @return array
     */
    public function attributeLabels()
    {
        return ArrayHelper::merge(parent::attributeLabels(), [
            'issuedMe' => 'Выданные мной',
        ]);
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = Bingo37Tickets::find()->joinWith(['cashier', 'deposit']);

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
            'pagination' => ['pageSize' => 10],
        ]);

        $dataProvider->sort->attributes['status']['default'] = SORT_DESC;

        $dataProvider->sort->attributes['cashier.fullName'] = [
            'asc'  => [User::tableName().'.fullName' => SORT_ASC],
            'desc' => [User::tableName().'.fullName' => SORT_DESC],
            'default' => SORT_DESC,
        ];

        $dataProvider->sort->attributes['deposit.amount'] = [
            'asc'  => [Bingo37TicketsHistory::tableName().'.amount' => SORT_ASC],
            'desc' => [Bingo37TicketsHistory::tableName().'.amount' => SORT_DESC],
        ];

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        $query->andFilterWhere([
            'id' => $this->id,
            'barcode' => $this->barcode,
            'created' => $this->created,
            'updated' => $this->updated,
            'status' => $this->status,
        ]);

        if ($this->issuedMe) {
            $query->andWhere(['cashierId' => Yii::$app->user->getId()]);
        }

        return $dataProvider;
    }
}
