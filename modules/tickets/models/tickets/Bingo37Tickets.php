<?php

namespace app\modules\tickets\models\tickets;

use app\models\users\User;
use app\modules\terminals\models\terminals\Bingo37Terminals;
use app\modules\tickets\models\tickets_actions\Bingo37TicketsActions;
use app\modules\tickets\models\tickets_history\Bingo37TicketsHistory;
use Yii;
use yii\behaviors\TimestampBehavior;
use yii\data\ActiveDataProvider;
use yii\db\Expression;

/**
 * This is the model class for table "{{%tickets}}".
 *
 * @property integer $id
 * @property integer $cashierId
 * @property integer $barcode
 * @property integer $created
 * @property integer $updated
 * @property integer $status
 * @property string  $activated
 * @property string  $serial_number
 *
 * @property User $cashier
 * @property Bingo37Terminals $terminal
 */
class Bingo37Tickets extends \yii\db\ActiveRecord
{
    const STATUS_OPENED = 0;
    const STATUS_GAMING = 1;
    const STATUS_AWAITING_PAYMENT = 2;
    const STATUS_CLOSED = 3;

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%tickets}}';
    }

    public function behaviors()
    {
        return [
            'timestamp' => [
                'class' => TimestampBehavior::className(),
                'createdAtAttribute' => 'created',
                'updatedAtAttribute' => 'updated',
                'value' => new Expression('NOW()'),
            ]
        ];
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['cashierId', 'status'], 'integer'],
            ['barcode', 'unique'],
            [['cashierId', 'created', 'updated', 'barcode', 'activated', 'serial_number'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'cashier.fullName' => 'Кассир',
            'deposit.amount' => 'Сумма взноса',
            'barcode' => 'Штрих-код',
            'created' => 'Создано',
            'updated' => 'Обновлено',
            'activated' => 'Активирован в терминале',
            'statusName' => 'Статус',
            'status' => 'Статус',
            'serial_number' => 'Серия/Номер'
        ];
    }

    /**
     * @return array
     */
    public function statusNames()
    {
        return [
            self::STATUS_OPENED => 'Новый',
            self::STATUS_GAMING => 'Активный',
            self::STATUS_AWAITING_PAYMENT => 'Ожидает оплаты',
            self::STATUS_CLOSED => 'Закрыт',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCashier()
    {
        return $this->hasOne(User::className(), ['id' => 'cashierId']);
    }

    /**
     * @inheritdoc
     * @return Bingo37TicketsQuery the active query used by this AR class.
     */
    public static function find()
    {
        return new Bingo37TicketsQuery(get_called_class());
    }

    /**
     * @return string
     */
    public function getStatusName()
    {
        return $this->statusNames()[$this->status];
    }

    /**
     * Returns the sum of the Deposit in connection with the
     * history table ticket
     *
     * @return $this
     */
    public function getDeposit()
    {
        return $this->hasOne(Bingo37TicketsHistory::className(), ['ticketId' => 'id'])
            ->where(['actionType' => Bingo37TicketsActions::TYPE_DEPOSIT]);
    }

    /**
     * Returns the total amount of the ticket, you have
     * to pay the player
     *
     * @return mixed
     */
    public function getTotalAmount()
    {
        return $this->getHistory()
            ->where('actionType <> :closed')
            ->params([':closed' => Bingo37TicketsHistory::ACTION_CONCLUSION])
            ->sum('amount');
    }

    /**
     * Returns the history of the life of a ticket :)
     *
     * @return \yii\db\ActiveQuery
     */
    public function getHistory()
    {
        return $this->hasMany(Bingo37TicketsHistory::className(), ['ticketId' => 'id']);
    }

    /**
     * This method is required for GridView
     *
     * @return ActiveDataProvider
     */
    public function getHistoryDataProvider()
    {
        return new ActiveDataProvider([
            'query' => $this->getHistory(),
            'sort' => false,
        ]);
    }

    /**
     * The terminal which is now the ticket, it can
     * be null
     *
     * @return \yii\db\ActiveQuery
     */
    public function getTerminal()
    {
        return $this->hasOne(Bingo37Terminals::className(), ['ticketId' => 'id']);
    }
}
