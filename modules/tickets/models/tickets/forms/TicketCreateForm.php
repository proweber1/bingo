<?php

namespace app\modules\tickets\models\tickets\forms;

use yii\base\Model;

/**
 * Class TicketCreateForm
 * @package app\modules\tickets\models\tickets\forms
 */
class TicketCreateForm extends Model
{
    /**
     * @var number
     */
    public $amount;

    /**
     * @return array
     */
    public function rules()
    {
        return [
            ['amount', 'required'],
            ['amount', 'number'],
        ];
    }

    /**
     * @return array
     */
    public function attributeLabels()
    {
        return ['amount' => 'Сумма депозита'];
    }
}