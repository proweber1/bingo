<?php

namespace app\modules\tickets\models\tickets;

/**
 * This is the ActiveQuery class for [[Bingo37Tickets]].
 *
 * @see Bingo37Tickets
 */
class Bingo37TicketsQuery extends \yii\db\ActiveQuery
{
    /*public function active()
    {
        $this->andWhere('[[status]]=1');
        return $this;
    }*/

    /**
     * @inheritdoc
     * @return Bingo37Tickets[]|array
     */
    public function all($db = null)
    {
        return parent::all($db);
    }

    /**
     * @inheritdoc
     * @return Bingo37Tickets|array|null
     */
    public function one($db = null)
    {
        return parent::one($db);
    }
}