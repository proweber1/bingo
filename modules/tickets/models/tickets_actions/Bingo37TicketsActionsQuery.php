<?php

namespace app\modules\tickets\models\tickets_actions;

/**
 * This is the ActiveQuery class for [[Bingo37TicketsActions]].
 *
 * @see Bingo37TicketsActions
 */
class Bingo37TicketsActionsQuery extends \yii\db\ActiveQuery
{
    /*public function active()
    {
        $this->andWhere('[[status]]=1');
        return $this;
    }*/

    /**
     * @inheritdoc
     * @return Bingo37TicketsActions[]|array
     */
    public function all($db = null)
    {
        return parent::all($db);
    }

    /**
     * @inheritdoc
     * @return Bingo37TicketsActions|array|null
     */
    public function one($db = null)
    {
        return parent::one($db);
    }
}