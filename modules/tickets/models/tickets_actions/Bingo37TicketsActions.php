<?php

namespace app\modules\tickets\models\tickets_actions;

use Yii;

/**
 * This is the model class for table "{{%tickets_actions}}".
 *
 * @property integer $id
 * @property string $name
 * @property string $alias
 *
 * @property TicketsHistory[] $ticketsHistories
 */
class Bingo37TicketsActions extends \yii\db\ActiveRecord
{
    const TYPE_DEPOSIT = 1;
    const TYPE_LOSS = 2;
    const TYPE_WIN = 3;
    const TYPE_CONCLUSION = 4;

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%tickets_actions}}';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['name', 'alias'], 'string', 'max' => 150]
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'name' => 'Name',
            'alias' => 'Alias',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getTicketsHistories()
    {
        return $this->hasMany(TicketsHistory::className(), ['actionType' => 'id']);
    }

    /**
     * @inheritdoc
     * @return Bingo37TicketsActionsQuery the active query used by this AR class.
     */
    public static function find()
    {
        return new Bingo37TicketsActionsQuery(get_called_class());
    }
}
