<?php

namespace app\modules\tickets\models\tickets_history;

/**
 * This is the ActiveQuery class for [[Bingo37TicketsHistory]].
 *
 * @see Bingo37TicketsHistory
 */
class Bingo37TicketsHistoryQuery extends \yii\db\ActiveQuery
{
    /*public function active()
    {
        $this->andWhere('[[status]]=1');
        return $this;
    }*/

    /**
     * @inheritdoc
     * @return Bingo37TicketsHistory[]|array
     */
    public function all($db = null)
    {
        return parent::all($db);
    }

    /**
     * @inheritdoc
     * @return Bingo37TicketsHistory|array|null
     */
    public function one($db = null)
    {
        return parent::one($db);
    }
}