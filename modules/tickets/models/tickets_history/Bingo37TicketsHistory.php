<?php

namespace app\modules\tickets\models\tickets_history;

use app\modules\terminals\models\terminals\Bingo37Terminals;
use app\modules\tickets\models\tickets\Bingo37Tickets;
use app\modules\tickets\models\tickets_actions\Bingo37TicketsActions;
use Yii;
use yii\behaviors\TimestampBehavior;
use yii\db\Expression;

/**
 * This is the model class for table "{{%tickets_history}}".
 *
 * @property integer $id
 * @property integer $ticketId
 * @property integer $terminalId
 * @property integer $actionType
 * @property string $amount
 * @property integer $created
 * @property integer $updated
 *
 * @property Bingo37TicketsActions $type
 * @property Tickets $ticket
 */
class Bingo37TicketsHistory extends \yii\db\ActiveRecord
{
    const ACTION_DEPOSITING = 1;
    const ACTION_LOSS = 2;
    const ACTION_WINN = 3;
    const ACTION_CONCLUSION = 4;

    public function historyActions()
    {
        return [
            self::ACTION_DEPOSITING => 'Пополнение счета',
            self::ACTION_LOSS => 'Проигрыш',
            self::ACTION_WINN => 'Выигрыш',
            self::ACTION_CONCLUSION => 'Вывод средств',
        ];
    }

    /**
     * @return array
     */
    public function behaviors()
    {
        return [
            'timestamp' => [
                'class' => TimestampBehavior::className(),
                'createdAtAttribute' => 'created',
                'updatedAtAttribute' => 'updated',
                'value' => new Expression('NOW()'),
            ]
        ];
    }

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%tickets_histories}}';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['ticketId', 'terminalId', 'actionType', 'amount'], 'required'],
            [['ticketId', 'terminalId', 'actionType', 'created', 'updated'], 'integer'],
            [['amount'], 'number'],
            [['created', 'updated'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'ticketId' => 'Номер билета',
            'terminalId' => 'Терминал',
            'type.alias' => 'Действие',
            'terminal.name' => 'Имя терминала',
            'amount' => 'Сумма',
            'created' => 'Создано',
            'updated' => 'Обновлено',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getType()
    {
        return $this->hasOne(Bingo37TicketsActions::className(), ['id' => 'actionType']);
    }

    /**
     * @inheritdoc
     * @return Bingo37TicketsHistoryQuery the active query used by this AR class.
     */
    public static function find()
    {
        return new Bingo37TicketsHistoryQuery(get_called_class());
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getTerminal()
    {
        return $this->hasOne(Bingo37Terminals::className(), ['id' => 'terminalId']);
    }

    public function getTicket()
    {
        return $this->hasOne(Bingo37Tickets::className(), ['id' => 'ticketId']);
    }
}
