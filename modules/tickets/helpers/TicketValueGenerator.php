<?php

namespace app\modules\tickets\helpers;

use DateTime;

/**
 * Class TicketValueGenerator
 * @package app\modules\tickets\helpers
 */
class TicketValueGenerator
{
    /**
     * @return string
     */
    public static function generateSerialNumber()
    {
        $dateTime = new DateTime();

        return $dateTime->format('dm') . ' ' . $dateTime->format('His');
    }
}