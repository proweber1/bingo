<?php

namespace app\modules\tickets\helpers;

/**
 * Class Ean13Calculator
 * @package app\modules\tickets\helpers
 */
class Ean13Calculator
{
    /**
     * @param $code
     * @return int
     */
    public static function calculate($code)
    {
        $code = str_pad($code, 12, "0", STR_PAD_LEFT);
        $sum = 0;
        for ($i = (strlen($code) - 1); $i >= 0; $i--) {
            $sum += (($i % 2) * 2 + 1 ) * $code[$i];
        }
        return (10 - ($sum % 10));
    }

    /**
     * @return string
     */
    public static function generateRandomBarcode()
    {
        // TODO: Сам блять знаю, ну а как иначе то?))
        $code = ((string) strlen(time()) > 10) ? time() * 10 : time() * 100;
        return $code . self::calculate($code);
    }
}