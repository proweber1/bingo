<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\modules\tickets\models\tickets\Bingo37Tickets */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="bingo37-tickets-form block">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'amount')->textInput(['maxlength' => true, 'autofocus' => 'autofocus']) ?>

    <?= Html::submitButton('Создать', ['class' => 'btn btn-primary create-ticket']) ?>
    <?= Html::a('Отмена', ['/site/index'], ['class' => 'btn btn-danger']) ?>

    <?php ActiveForm::end(); ?>

</div>
