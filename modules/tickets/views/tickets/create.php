<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model app\modules\tickets\models\tickets\Bingo37Tickets */

$this->title = 'Создать билет';
$this->params['breadcrumbs'][] = ['label' => 'Билеты', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>

<h2><?= Html::encode($this->title) ?></h2>

<div class="bingo37-tickets-create">

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
