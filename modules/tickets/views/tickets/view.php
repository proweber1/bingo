<?php

use yii\helpers\Html;
use yii\widgets\DetailView;
use yii\grid\GridView;
use app\modules\tickets\models\tickets\Bingo37Tickets;

/* @var $this yii\web\View */
/* @var $model app\modules\tickets\models\tickets\Bingo37Tickets */

$isClosedTicket = $model->status === Bingo37Tickets::STATUS_CLOSED;

$this->title = $model->barcode;
$this->params['breadcrumbs'][] = ['label' => 'Bingo37 Tickets', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>

<h2><?= Html::encode($this->title) ?></h2>

<div class="bingo37-tickets-view block">

    <p>
        <button class="print btn btn-success">Распечатать билет</button>
        <?php if (!$isClosedTicket && $model->status === Bingo37Tickets::STATUS_AWAITING_PAYMENT) : ?>
            <?= Html::a('Выплатить: '.number_format($model->totalAmount, 2).' KZT', ['finish', 'id' => $model->id], [
                'class' => 'btn btn-success without-button'
            ]); ?>
        <?php endif; ?>
    </p>

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'barcode',
            'deposit.amount',
            'cashier.fullName',
            'created:dateTime',
            'updated:dateTime',
            'activated:dateTime',
            'statusName',
            'serial_number',
        ],
    ]) ?>

    <?= $this->render('ticket', [
        'amount' => $model->deposit->amount,
        'barcode' => $model->barcode,
        'author' => $model->cashier->fullName,
        'serialNumber' => $model->serial_number,
        'status' => $isClosedTicket,
    ]); ?>

    <h3>История билета</h3>

    <?= GridView::widget([
        'dataProvider' => $model->getHistoryDataProvider(),
        'columns' => [
            [
                'attribute' => 'terminal.name',
                'value' => function ($model) {
                    return isset($model->terminal)
                        ? Html::a($model->terminal->getName(), ['/terminals/crud/view', 'id' => $model->terminal->id])
                        : 'Касса';
                },
                'format' => 'html',
            ],
            'type.alias',
            'amount',
            'created:datetime',
        ]
    ]); ?>

</div>
