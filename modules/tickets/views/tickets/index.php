<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel app\modules\tickets\models\tickets\Bingo37TicketsSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Билеты';
$this->params['breadcrumbs'][] = $this->title;
?>

<h2><?= Html::encode($this->title) ?></h2>

<div class="bingo37-tickets-index block">

    <?php echo $this->render('_search', ['model' => $searchModel]); ?>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            'deposit.amount',
            'barcode',
            'created:dateTime',
            'activated:dateTime',
            'serial_number',
            [
                // TODO: So we had to do to bother with sort of a complex field
                'attribute' => 'status',
                'value' => function ($model) {
                    /** @var $model \app\modules\tickets\models\tickets\Bingo37Tickets */
                    return $model->statusName;
                },
            ],

            [
                'class' => 'yii\grid\ActionColumn',
                'template' => '{view}',
            ],
        ],
        'tableOptions' => ['class' => 'table'],
    ]); ?>

</div>
