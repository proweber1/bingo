<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\modules\tickets\models\tickets\Bingo37TicketsSearch */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="bingo37-tickets-search">

    <?php $form = ActiveForm::begin([
        'action' => ['index'],
        'method' => 'get',
    ]); ?>

    <?= $form->field($model, 'barcode') ?>

    <?= $form->field($model, 'status')->dropDownList($model->statusNames(), ['prompt' => 'Все']) ?>

    <?= $form->field($model, 'issuedMe')->checkbox() ?>

    <div class="form-group">
        <?= Html::submitButton('Найти', ['class' => 'btn btn-primary']) ?>
        <?= Html::resetButton('Очистить', ['class' => 'btn btn-default']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
