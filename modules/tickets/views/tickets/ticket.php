<?php

/* @var $this yii\web\View */
/* @var int $barcode */
/* @var bool $status */
/* @var number $amount */
/* @var string $author */
/* @var string $serialNumber */

use yii\helpers\Html;
use barcode\barcode\BarcodeGenerator;

?>

<div class="block">

    <div class="media" id="ticket">
        <div class="media-left media-middle">
            <div id="barcode" class="pull-left"></div>
            <?= BarcodeGenerator::widget([
                'elementId' => 'barcode',
                'type'      => 'ean13',
                'value'     => $barcode,
//                'rectangular' => true,
                'settings'  => [
                    'barWidth' => 2,
                    'barHeight' => 80,
                ],
            ]); ?>
        </div>
        <div class="media-body text-center text-uppercase">

            <?php if ($status) : ?>
                <div class="complete-ticket">
                    <?= Html::img(Yii::getAlias('@web').'/images/nashlepka.png', ['width' => 350]); ?>
                </div>
            <?php endif; ?>

            <div class="row">
                <div class="col-xs-4"></div>
                <div class="col-xs-4">
                    <h1 class="ticket-title"><?= Html::encode(Yii::$app->settings->get('Ticket.casinoName')); ?></h1>
                </div>
                <div class="col-xs-4 text-right" style="padding-top: 19px;">
                    <?= $serialNumber; ?>
                </div>
            </div>

            <p><?= Html::encode(Yii::$app->settings->get('Ticket.casinoInformation')); ?></p>
            <div class="row">
                <div class="col-xs-4 text-left">
                    <h5><?= Html::encode(Yii::$app->settings->get('Ticket.phone')); ?></h5>
                </div>
                <div class="col-xs-4">
                    <h5><?= Html::encode(Yii::$app->settings->get('Ticket.address')); ?></h5>
                </div>
                <div class="col-xs-4 text-right">
                    <h5><?= Html::encode(Yii::$app->settings->get('Ticket.site')); ?></h5>
                </div>
            </div>

            <h1 class="ticket-amount"><?= number_format($amount, 2); ?> kzt</h1>

            <div class="row">
                <div class="col-xs-6">
                    <p class="text-left">
                        <?= Html::encode(Yii::$app->settings->get('Ticket.ticketInformation')); ?>
                    </p>
                </div>
                <div class="col-xs-6 text-right">
                    <p>Администратор: <?= Html::encode($author); ?></p>
                    <p><?= date('d.m.Y H:i'); ?></p>
                </div>
            </div>
        </div>
        <hr />
    </div>
</div>