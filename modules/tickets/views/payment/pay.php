<?php

/** @var yii\web\View $this */

use yii\helpers\Html;

$this->title = 'Выплатить по билету';
$this->params['breadcrumbs'][] = $this->title;

?>

<h2>Выплата по билету</h2>

<div class="block">
    <div class="avgrund"></div>

    <?= Html::beginForm(false, false, ['id' => 'ticket-payment']); ?>

    <div class="form-group">
        <?= Html::label('Введите штрих-код'); ?>
        <?= Html::textInput('barcode', '', [
            'id' => 'barcodeNumber',
            'class' => 'form-control',
            'required' => 'required',
            'autofocus' => 'autofocus',
        ]); ?>
    </div>

    <?= Html::submitButton('Найти', ['class' => 'btn btn-primary']); ?>

    <?= Html::a('Отмена', ['/site/index'], ['class' => 'btn btn-danger']); ?>

    <?= Html::endForm(); ?>
</div>