<?php

namespace app\modules\tickets\controllers;

use app\modules\tickets\controllers\system\SecurityController;

/**
 * Class PaymentController
 * @package app\modules\tickets\controllers
 */
class PaymentController extends SecurityController
{
    /**
     * @var string
     */
    public $defaultAction = 'pay';

    /**
     * @return string
     */
    public function actionPay()
    {
        return $this->render('pay');
    }
}