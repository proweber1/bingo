<?php

namespace app\modules\tickets\controllers\system;

use app\models\users\User;

/**
 * Class SecurityController
 * @package app\modules\tickets\controllers\system
 */
class SecurityController extends \app\controllers\system\SecurityController
{
    /**
     * @return array
     */
    public function accessRules()
    {
        return [
            ['allow' => true, 'roles' => [User::CASHIER_ROLE]],
        ];
    }
}