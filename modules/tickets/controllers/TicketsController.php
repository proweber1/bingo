<?php

namespace app\modules\tickets\controllers;

use app\modules\tickets\models\tickets\forms\TicketCreateForm;
use Yii;
use app\modules\tickets\controllers\system\SecurityController;
use app\modules\tickets\models\tickets\Bingo37Tickets;
use app\modules\tickets\models\tickets\Bingo37TicketsSearch;
use yii\web\NotFoundHttpException;

/**
 * TicketsController implements the CRUD actions for Bingo37Tickets model.
 */
class TicketsController extends SecurityController
{
    /**
     * Lists all Bingo37Tickets models.
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new Bingo37TicketsSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * @param $id
     */
    public function actionFinish($id)
    {
        /** @var \app\services\tickets\TicketStatesService $service */
        $service = Yii::$container->get('app\services\tickets\TicketStatesService');

        $service->attach(Yii::$container->get('app\services\tickets\observers\ConclusionObServer'));
        $service->attach(Yii::$container->get('app\services\tickets\observers\NodeNotifyOnClosedTicket'));

        if ($service->finishTicket($id)) {
            $this->redirect(['view', 'id' => $id]);
        }

        // TODO: To figure out what to do in this case
    }

    /**
     * Displays a single Bingo37Tickets model.
     * @param integer $id
     * @return mixed
     */
    public function actionView($id)
    {
        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
    }

    /**
     * Creates a new Bingo37Tickets model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        /** @var \app\services\tickets\TicketService $ticketsService */
        $ticketsService = Yii::$container->get('app\services\tickets\TicketService');
        $ticketsService->attach(Yii::$container->get('app\services\tickets\observers\AddDepositObServer'));

        $ticketsCreateForm = new TicketCreateForm();

        if ($ticketsCreateForm->load(Yii::$app->request->post()) && $ticketsCreateForm->validate()) {
            if ($ticketsService->createTicket($ticketsCreateForm)) {
                return $this->redirect(['view', 'id' => $ticketsService->getModel()->id]);
            }
        }

        return $this->render('create', [
            'model' => $ticketsCreateForm,
        ]);
    }

    /**
     * Finds the Bingo37Tickets model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return Bingo37Tickets the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = Bingo37Tickets::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }
}
