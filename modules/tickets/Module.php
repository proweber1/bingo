<?php

namespace app\modules\tickets;

/**
 * Class Module
 * @package app\modules\tickets
 */
class Module extends \yii\base\Module
{
    /**
     * @var string
     */
    public $controllerNamespace = 'app\modules\tickets\controllers';

    /**
     * @var string
     */
    public $defaultRoute = 'tickets/index';

    public function init()
    {
        parent::init();

        // custom initialization code goes here
    }
}
