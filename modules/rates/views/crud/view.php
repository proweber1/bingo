<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model app\modules\rates\models\rates\Bingo37RatesTypes */

$this->title = $model->name;
$this->params['breadcrumbs'][] = ['label' => 'Bingo37 Rates Types', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>

<h2><?= Html::encode($this->title) ?></h2>

<div class="bingo37-rates-types-view block">

    <p>
        <?= Html::a('Update', ['update', 'id' => $model->id], ['class' => 'btn btn-primary']) ?>
    </p>

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'name',
            'min',
            'max',
        ],
    ]) ?>

</div>
