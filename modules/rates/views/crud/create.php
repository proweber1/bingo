<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model app\modules\rates\models\rates\Bingo37RatesTypes */

$this->title = 'Create Bingo37 Rates Types';
$this->params['breadcrumbs'][] = ['label' => 'Bingo37 Rates Types', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>

<h2><?= Html::encode($this->title) ?></h2>

<div class="bingo37-rates-types-create block">

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
