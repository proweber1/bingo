<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\modules\rates\models\rates\Bingo37RatesTypes */

$this->title = 'Update Bingo37 Rates Types: ' . ' ' . $model->name;
$this->params['breadcrumbs'][] = ['label' => 'Bingo37 Rates Types', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->name, 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = 'Update';
?>

<h2><?= Html::encode($this->title) ?></h2>

<div class="bingo37-rates-types-update block">

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
