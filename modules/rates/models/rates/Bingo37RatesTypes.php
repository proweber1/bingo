<?php

namespace app\modules\rates\models\rates;

use Yii;

/**
 * This is the model class for table "{{%rates_types}}".
 *
 * @property integer $id
 * @property string $name
 * @property string $min
 * @property string $max
 *
 * @property GameRates[] $gameRates
 */
class Bingo37RatesTypes extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%rates_types}}';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['name'], 'required'],
            [['min', 'max'], 'number'],
            [['name'], 'string', 'max' => 180],
            [['name'], 'unique']
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'name' => 'Name',
            'min' => 'Min',
            'max' => 'Max',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getGameRates()
    {
        return $this->hasMany(GameRates::className(), ['rateType' => 'id']);
    }

    /**
     * @inheritdoc
     * @return Bingo37RatesTypesQuery the active query used by this AR class.
     */
    public static function find()
    {
        return new Bingo37RatesTypesQuery(get_called_class());
    }
}
