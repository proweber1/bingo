<?php

namespace app\modules\rates\models\rates;

/**
 * This is the ActiveQuery class for [[Bingo37RatesTypes]].
 *
 * @see Bingo37RatesTypes
 */
class Bingo37RatesTypesQuery extends \yii\db\ActiveQuery
{
    /*public function active()
    {
        $this->andWhere('[[status]]=1');
        return $this;
    }*/

    /**
     * @inheritdoc
     * @return Bingo37RatesTypes[]|array
     */
    public function all($db = null)
    {
        return parent::all($db);
    }

    /**
     * @inheritdoc
     * @return Bingo37RatesTypes|array|null
     */
    public function one($db = null)
    {
        return parent::one($db);
    }
}