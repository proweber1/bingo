<?php

namespace app\modules\rates;

class Module extends \yii\base\Module
{
    public $controllerNamespace = 'app\modules\rates\controllers';

    public function init()
    {
        parent::init();

        // custom initialization code goes here
    }
}
