<?php

namespace app\modules\statistics\forms;

use Yii;
use app\components\ArrayHelper;
use yii\base\Model;
use app\modules\statistics\reports\DateRangeInterface;

/**
 * Class BallDateRangeForm
 * @package app\modules\statistics\forms
 */
class BallDateRangeForm extends Model implements DateRangeInterface
{
    const TERMINALS_STATISTIC = 'terminal stat';

    /**
     * @var string
     */
    public $dateFrom;
    /**
     * @var string
     */
    public $dateTo;
    /**
     * @var string
     */
    public $explodeOnDays;
    /**
     * @var array
     */
    public $terminals;

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            ['dateFrom', 'required', 'on' => self::SCENARIO_DEFAULT],
            ['terminals', 'required', 'on' => self::TERMINALS_STATISTIC],
            [['dateFrom', 'dateTo'], 'string'],
            ['explodeOnDays', 'integer'],
            ['terminals', 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'dateFrom' => 'От',
            'dateTo' => 'До',
            'explodeOnDays' => 'Разбить по дням',
            'terminals' => 'Терминалы',
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        return ArrayHelper::merge(parent::scenarios(), [
            self::TERMINALS_STATISTIC => [
                'terminals',
            ]
        ]);
    }

    /**
     * @inheritdoc
     */
    public function getDateFrom()
    {
        return $this->dateFrom;
    }

    /**
     * @inheritdoc
     */
    public function getDateTo()
    {
        return $this->dateTo;
    }

    /**
     * @inheritdoc
     */
    public function setDateFrom($value)
    {
        $this->dateFrom = $value;
    }

    /**
     * @inheritdoc
     */
    public function setDateTo($value)
    {
        $this->dateTo = $value;
    }

    /**
     * @inheritdoc
     */
    public function setExplodeOnDays($value)
    {
        return $this->explodeOnDays = $value;
    }

    /**
     * @inheritdoc
     */
    public function getExplodeOnDays()
    {
        return $this->explodeOnDays;
    }

    /**
     * @inheritdoc
     */
    public function getSelectedTerminals()
    {
        return $this->terminals;
    }

    /**
     * @inheritdoc
     */
    public function getTerminalsList()
    {
        /** @var \app\services\terminals\TerminalService $ts */
        $ts = Yii::$container->get('app\services\terminals\TerminalService');
        $terminals = $ts->getTerminals();

        return ArrayHelper::map($terminals, 'id', 'name');
    }
}