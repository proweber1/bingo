<?php

namespace app\modules\statistics\reports;

use app\modules\statistics\helpers\LototronHelper;
use app\modules\statistics\helpers\RadarGraphHelper;

/**
 * Class StatisricReport
 * @package app\modules\statistics\reports
 */
class StatisricReport extends AbstractReport implements ReportInterface
{
    /**
     * @inheritdoc
     */
    public function report()
    {
        $data = $this->getService()->getReportData($this->getForm());

        return [
            'items' => $data,
            'radarChart' => [
                'labels' => range(0, 36),
                'values' => RadarGraphHelper::normalizeResult($data),
                'averageValues' => RadarGraphHelper::averageValues($data),
            ],
            'table' => LototronHelper::statisticTableData($data),
        ];
    }
}