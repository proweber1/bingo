<?php

namespace app\modules\statistics\reports;
use app\modules\tickets\models\tickets_history\Bingo37TicketsHistory;
use yii\helpers\ArrayHelper;

/**
 * Class AmountExplodeOnDaysReport
 * @package app\modules\statistics\reports
 */
class AmountExplodeOnDaysReport extends AbstractReport implements ReportInterface
{
    /**
     * @inheritdoc
     */
    public function report()
    {
        $items = $this->getService()->getReportData($this->getForm());

        return [
            'items' => $this->itemsPrepare($items),
        ];
    }

    /**
     * Preparation of a list of items to display in the report
     *
     * @param array $items
     * @return array
     */
    private function itemsPrepare(array $items)
    {
        $columns = ArrayHelper::getColumn($items, 'created');

        $normalResult = $this->normalizeItemsArray($columns, $items);
        $this->addInOut($normalResult);

        return $normalResult;
    }

    /**
     * Normalization of array with elements from the request,
     * for it was all right to calculate and display the result
     *
     * @param array $columns
     * @param array $items
     * @param array $out
     * @return array
     */
    private function normalizeItemsArray(array $columns, array $items, $out = [])
    {
        foreach ($columns as $column) {
            // Todo: Гениальный костыль ))
            $out[$column] = [
                'id' => 0,
                'date' => '',
                'in' => 0,
                'out' => 0,
            ];

            $this->eachItems($column, $items, $out);
        }

        return $out;
    }

    /**
     * Looping all elements of the array $items and
     * return a formatted result
     *
     * @param string $column
     * @param array $items Да, это ссылка на массив $out, так пришлось сделать, плохая практика
     * @param array $out
     */
    private function eachItems($column, array $items, array &$out)
    {
        foreach ($items as $item) {
            if ($item['created'] !== $column) {
                continue;
            }

            $out[$column]['id'] = $item['id'];
            $out[$column]['date'] = $item['created'];

            // todo: refactor this code
            if ((int)$item['actionType'] === Bingo37TicketsHistory::ACTION_DEPOSITING) {
                $out[$column]['in'] = $item['amount'];
                continue;
            }

            if ((int)$item['actionType'] === Bingo37TicketsHistory::ACTION_CONCLUSION) {
                $out[$column]['out'] = $item['amount'];
                continue;
            }
        }
    }

    /**
     * The profit calculation is a simple difference of two numbers
     *
     * @param array $out
     * @return array
     */
    private function addInOut(array &$out)
    {
        foreach ($out as &$out_item) {
            $out_item['in_out'] = $out_item['in'] - $out_item['out'];
        }

        return $out;
    }
}