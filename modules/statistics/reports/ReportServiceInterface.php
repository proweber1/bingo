<?php

namespace app\modules\statistics\reports;

/**
 * Interface ReportServiceInterface
 * @package app\modules\statistics\reports
 */
interface ReportServiceInterface
{
    /**
     * Returns statistics
     *
     * @param DateRangeInterface $form
     * @return mixed
     */
    public function getReportData(DateRangeInterface $form);

    /**
     * Return self report object
     *
     * @param DateRangeInterface $form
     * @return ReportInterface
     */
    public function getReportObject(DateRangeInterface $form);
}