<?php

namespace app\modules\statistics\reports;

/**
 * Class AbstractReport
 * @package app\modules\statistics\reports
 */
abstract class AbstractReport implements ReportInterface
{
    /**
     * @var ReportServiceInterface
     */
    private $service;

    /**
     * @return ReportServiceInterface
     */
    public function getService()
    {
        return $this->service;
    }

    /**
     * @var DateRangeInterface
     */
    private $form;

    /**
     * @return DateRangeInterface
     */
    public function getForm()
    {
        return $this->form;
    }

    /**
     * AbstractReport constructor.
     *
     * @param ReportServiceInterface $service
     * @param DateRangeInterface $form
     */
    public function __construct(ReportServiceInterface $service, DateRangeInterface $form)
    {
        $this->service = $service;
        $this->form = $form;
    }
}