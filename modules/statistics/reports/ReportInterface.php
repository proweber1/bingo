<?php

namespace app\modules\statistics\reports;

/**
 * Interface ReportInterface
 * @package app\modules\statistics\reports
 */
interface ReportInterface
{
    /**
     * @return array
     */
    public function report();
}