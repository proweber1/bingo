<?php

namespace app\modules\statistics\reports;

use app\modules\tickets\models\tickets_history\Bingo37TicketsHistory;

/**
 * Class TerminalsReport
 * @package app\modules\statistics\reports
 */
class TerminalsReport extends AbstractReport
{
    /**
     * @inheritdoc
     */
    public function report()
    {
        $reportData = $this->getService()->getReportData($this->getForm());

        return [
            'all_rates' => count($reportData),
            'all_loos' => $this->getSumByType($reportData, Bingo37TicketsHistory::ACTION_LOSS),
            'all_wins' => $this->getSumByType($reportData, Bingo37TicketsHistory::ACTION_WINN),
            'all_bets' => $this->getAllSum($reportData),
            'western_day' => $this->getWesternDayData($reportData),
            'all_days_data' => $reportData,
        ];
    }

    /**
     * Возвращает статистику за сегодняшний день
     *
     * @param array $reportData
     * @return array
     */
    private function getWesternDayData($reportData)
    {
        $selfDate = (new \DateTime())->format('Y-m-d');

        $out = array_filter($reportData, function ($var) use ($selfDate) {
            return $var['created'] === $selfDate;
        });

        return [
            'western_day_count' => count($out),
            'western_day_data' => $out,
            'western_day_wins' => $this->getSumByType($out, Bingo37TicketsHistory::ACTION_WINN),
            'western_day_loos' => $this->getSumByType($out, Bingo37TicketsHistory::ACTION_LOSS),
            'western_day_all_bets' => $this->getAllSum($out),
        ];
    }

    /**
     * Считает сумму в массиве
     *
     * @param array $data
     * @return float|integer
     */
    public function getAllSum(array $data)
    {
        return array_sum(array_column($data, 'amount'));
    }

    /**
     * Считает сумму по типу экшена
     *
     * @param array $data
     * @param int $type
     * @return int
     */
    private function getSumByType(array $data, $type)
    {
        $out = array_filter($data, function ($var) use ($type) {
            return (int) $var['actionType'] === $type;
        });

        return $this->getAllSum($out);
    }
}