<?php

namespace app\modules\statistics\reports;

/**
 * Interface DateRangeInterface
 * @package app\modules\statistics\reports
 */
interface DateRangeInterface
{
    /**
     * Return date from
     *
     * @return string
     */
    public function getDateFrom();

    /**
     * Return date to
     *
     * @return string
     */
    public function getDateTo();

    /**
     * Setter date from
     *
     * @param string|int $value
     * @return void
     */
    public function setDateFrom($value);

    /**
     * Setter date to
     *
     * @param string|int $value
     * @return void
     */
    public function setDateTo($value);

    /**
     * Get explode on days property
     *
     * @return boolean
     */
    public function getExplodeOnDays();

    /**
     * Setter explode on days
     *
     * @param bool $value
     * @return void
     */
    public function setExplodeOnDays($value);

    /**
     * Return terminal array list
     *
     * @return array
     */
    public function getTerminalsList();

    /**
     * Return selected terminals
     *
     * @return array|integer
     */
    public function getSelectedTerminals();
}