<?php

namespace app\modules\statistics\reports;

use app\modules\tickets\models\tickets_history\Bingo37TicketsHistory;

/**
 * Class AmountReport
 * @package app\modules\statistics\reports
 */
class AmountReport extends AbstractReport implements ReportInterface
{
    /**
     * @inheritdoc
     */
    public function report()
    {
        $data = $this->getService()->getReportData($this->getForm());

        $total_in = $this->getTotalByType($data);
        $total_out = $this->getTotalByType($data, Bingo37TicketsHistory::ACTION_CONCLUSION);

        return [
            'items' => $data,
            'total_in'  => $total_in,
            'total_out' => $total_out,
            'total_difference' => $total_in - $total_out,
        ];
    }

    /**
     * Considers the amount depending on the desired
     * type (profit, payment and so on)
     *
     * @param array $data
     * @param int $type
     * @param int $out
     * @return int
     */
    private function getTotalByType(array $data, $type = Bingo37TicketsHistory::ACTION_DEPOSITING, $out = 0)
    {
        foreach ($data as $data_item) {
            if ((int) $data_item['actionType'] === $type) {
                $out += $data_item['amount'];
            }
        }

        return $out;
    }
}