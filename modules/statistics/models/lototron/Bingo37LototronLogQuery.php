<?php

namespace app\modules\statistics\models\lototron;

/**
 * This is the ActiveQuery class for [[Bingo37LototronLog]].
 *
 * @see Bingo37LototronLog
 */
class Bingo37LototronLogQuery extends \yii\db\ActiveQuery
{
    /*public function active()
    {
        $this->andWhere('[[status]]=1');
        return $this;
    }*/

    /**
     * @inheritdoc
     * @return Bingo37LototronLog[]|array
     */
    public function all($db = null)
    {
        return parent::all($db);
    }

    /**
     * @inheritdoc
     * @return Bingo37LototronLog|array|null
     */
    public function one($db = null)
    {
        return parent::one($db);
    }
}