<?php

namespace app\modules\statistics\models\lototron;

use Yii;

/**
 * This is the model class for table "{{%lototron_log}}".
 *
 * @property integer $id
 * @property integer $number
 * @property integer $createdAt
 * @property integer $updatedAt
 */
class Bingo37LototronLog extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%lototron_log}}';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['number'], 'required'],
            [['number', 'createdAt', 'updatedAt'], 'integer']
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'number' => 'Number',
            'createdAt' => 'Created At',
            'updatedAt' => 'Updated At',
            'thisCount' => 'Количество выпаданий',
        ];
    }

    /**
     * @inheritdoc
     * @return Bingo37LototronLogQuery the active query used by this AR class.
     */
    public static function find()
    {
        return new Bingo37LototronLogQuery(get_called_class());
    }
}
