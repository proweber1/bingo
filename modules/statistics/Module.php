<?php

namespace app\modules\statistics;

class Module extends \yii\base\Module
{
    public $controllerNamespace = 'app\modules\statistics\controllers';

    public function init()
    {
        parent::init();

        // custom initialization code goes here
    }
}
