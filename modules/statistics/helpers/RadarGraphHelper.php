<?php

namespace app\modules\statistics\helpers;

class RadarGraphHelper
{
    /**
     * Normalizes the result so that it was clear radar
     * chart js
     *
     * @param array $records
     * @param array $out
     * @return array
     */
    public static function normalizeResult(array $records, $out = [])
    {
        // todo: Говнокод получился, надо будет пофиксить
        for ($i = 0; $i < 37; $i++) {
            $itemNumber = 0;

            foreach ($records as $item) {
                if ((int) $item['number'] === $i) {
                    $itemNumber = $item['thisCount'];
                }
            }

            $out[] = $itemNumber;
        }

        return $out;
    }

    /**
     * The average number of all the elements for the graph
     *
     * @param array $records
     * @return float
     */
    public static function averageValues(array $records)
    {
        $items = self::normalizeResult($records);

        return array_fill(0, 37, ceil(array_sum($items) / count($items)));
    }
}