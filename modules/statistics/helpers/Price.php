<?php

namespace app\modules\statistics\helpers;

/**
 * Class Price
 * @package app\modules\statistics\helpers
 */
class Price
{
    /**
     * Converts the amount in the correct format website
     *
     * @param double|int $number
     * @return string
     */
    public static function format($number)
    {
        if (!is_numeric($number)) {
            throw new \InvalidArgumentException('Number not is numeric');
        }

        return number_format($number, 2);
    }
}