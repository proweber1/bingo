<?php

namespace app\modules\statistics\helpers;

class LototronHelper
{
    /**
     * @param array $records
     * @return array
     */
    public static function statisticTableData(array $records)
    {
        $total = self::totalCount($records);

        foreach ($records as &$record) {
            $record['percent'] = ($record['thisCount'] * 100) / $total;
        }

        return $records;
    }

    /**
     * @param array $records
     * @param int $sum
     * @return int
     */
    private static function totalCount(array $records, $sum = 0)
    {
        foreach ($records as $record) {
            $sum += $record['thisCount'];
        }

        return $sum;
    }
}