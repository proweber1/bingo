<?php

use yii\widgets\ActiveForm;
use yii\bootstrap\Html;

/** @var yii\web\View $this */
/** @var \app\modules\statistics\reports\DateRangeInterface $form */

$this->title = 'Статистика по терминалам';

$this->params['breadcrumbs'][] = $this->title;

?>

<h2 class="page-title"><?= $this->title; ?></h2>

<div class="block">
    <?php $af = ActiveForm::begin([
        'method' => 'get',
    ]); ?>

    <?= $af->field($form, 'terminals')->listBox($form->getTerminalsList()); ?>

    <?= Html::submitButton('Получить', ['class' => 'btn btn-success']); ?>

    <?php ActiveForm::end(); ?>
</div>

<?php if (isset($data) && count($data, COUNT_RECURSIVE) > 0): ?>
    <br/><br/>
    <h2 class="page-title">Статистика за сегодняшний день</h2>
    <div class="block">
        <?php if (!isset($data['western_day'])): ?>
            <p>За сегодня статистики нет</p>
        <?php else: ?>
            <h4>Всего сделали ставок за сегодня: <?= Html::encode($data['western_day']['western_day_count']); ?></h4>
            <h4>TOTAL WINNING BETS: <?= number_format($data['western_day']['western_day_wins']); ?></h4>
<!--            <h4>Сегодня проиграли: --><?//= number_format(abs($data['western_day']['western_day_loos'])); ?><!--</h4>-->
            <h4>TOTAL BETS: <?= number_format($data['western_day']['western_day_all_bets']); ?></h4>

            <h3>Информация по ставкам за сегодня</h3>
            <?= $this->render('dataTable', [
                'data' => $data['western_day']['western_day_data'],
                'actionTypes' => $actionTypes
            ]); ?>
        <?php endif; ?>
    </div>

    <br><br>

    <h2 class="page-title">Статистика за все время</h2>
    <div class="block">
        <h4>Всего ставок сделали за все время: <?= Html::encode($data['all_rates']); ?></h4>
        <h4>TOTAL WINNING BETS: <?= number_format($data['all_wins']); ?></h4>
<!--        <h4>Всего проиграли: --><?//= number_format(abs($data['all_loos'])); ?><!--</h4>-->
        <h4>TOTAL BETS: <?= number_format($data['all_bets']); ?></h4>

        <h3>Информация по ставкам за все время</h3>
        <?= $this->render('dataTable', [
            'data' => $data['all_days_data'],
            'actionTypes' => $actionTypes,
        ]); ?>
    </div>
<?php endif; ?>