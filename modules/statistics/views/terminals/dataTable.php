<?php

/** @var yii\web\View $this */
/** @var [] $data */
/** @var [] $actionTypes */

use yii\helpers\Html;

$formatter = new \yii\i18n\Formatter();

?>

<table class="table table-striped table-hover">
    <thead>
    <tr>
        <th>Билет</th>
        <th>Действие</th>
        <th>Сумма</th>
        <th>Дата</th>
    </tr>
    </thead>
    <tbody>
        <?php foreach ($data as $item): ?>
            <tr>
                <td>
                    <?= Html::a(Html::encode($item['ticket']['barcode']), [
                        '/tickets/tickets/view', 'id' => $item['ticket']['id']
                    ]); ?>
                </td>
                <td><?= Html::encode($actionTypes[$item['actionType']]); ?></td>
                <td><?= $item['amount']; ?></td>
                <td><?= $formatter->format($item['created'], 'date'); ?></td>
            </tr>
        <?php endforeach; ?>
    </tbody>
</table>