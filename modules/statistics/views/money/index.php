<?php

use yii\widgets\ActiveForm;
use kartik\date\DatePicker;
use yii\helpers\Html;
use app\components\ArrayHelper;
use app\modules\statistics\helpers\Price;

/** @var yii\web\View $this */
/** @var \app\modules\statistics\reports\DateRangeInterface $form */

$this->title = 'Отчет по финансам';
$this->params['breadcrumbs'][] = $this->title;

?>

<h2 class="page-title"><?= $this->title; ?></h2>

<div class="statistics-default-index block">

    <span>Выберите промежуток дат для отчета.</span>

    <?php $formWidget = ActiveForm::begin(['method' => 'get']); ?>

    <?= DatePicker::widget([
        'separator' => 'До',
        'model' => $form,
        'form' => $formWidget,
        'attribute' => 'dateFrom',
        'attribute2' => 'dateTo',
        'type' => DatePicker::TYPE_RANGE,
        'pluginOptions' => [
            'format' => 'yyyy-mm-dd',
        ]
    ]); ?>

    <br /><?= $formWidget->field($form, 'explodeOnDays')->checkbox(); ?>

    <?= Html::submitButton('Найти', ['class' => 'btn btn-success btn-block']); ?>
    <?php ActiveForm::end(); ?>
</div> <br /><br />

<?php if (!is_null($data) && count($data['items']) > 0): ?>

    <h2 class="page-title">Отчет</h2>

    <?php if (ArrayHelper::keyExistsR('date', $data['items'])): ?>
        <?= $this->render('_table', [
            'items' => $data['items']
        ]); ?>
    <?php else: ?>
        <div class="block text-center">

            <canvas id="myChart" width="800" height="400"></canvas>

            <script>
                var ctx = $('#myChart').get(0).getContext('2d');
                var myBarChart = new Chart(ctx).Bar({
                    labels: ["Total in", "Total out", "Total in - Total out"],
                    datasets: [
                        {
                            label: "My First dataset",
                            fillColor: "rgba(151,187,205,0.5)",
                            strokeColor: "rgba(151,187,205,0.8)",
                            highlightFill: "rgba(151,187,205,0.75)",
                            highlightStroke: "rgba(151,187,205,1)",
                            data: [<?= $data['total_in']; ?>, <?= $data['total_out']; ?>, <?= $data['total_difference']; ?>]
                        }
                    ]
                });
            </script>

            <br /><br />

            <table class="table table-striped table-hover text-left">
                <thead>
                    <tr>
                        <th>TOTAL IN</th>
                        <th>TOTAL OUT</th>
                        <th>TOTAL IN - TOTAL OUT</th>
                    </tr>
                </thead>
                <tbody>
                    <tr>
                        <td><?= number_format($data['total_in']); ?></td>
                        <td><?= number_format($data['total_out']); ?></td>
                        <td><?= number_format($data['total_in'] - $data['total_out']); ?></td>
                    </tr>
                </tbody>
            </table>
        </div>
    <?php endif; ?>
<?php endif; ?>
