<?php

use yii\helpers\Html;
use app\modules\statistics\helpers\Price;

$formatter = new \yii\i18n\Formatter();

/** @var [] $items */

?>

<?php foreach ($items as $item): ?>
    <div class="block">

        <canvas id="myChart-<?= Html::encode($item['id']); ?>" width="800" height="400"></canvas>

        <script>
            var selector = '#myChart-' + <?= Html::encode($item['id']); ?>;
            var ctx = $(selector).get(0).getContext('2d');
            var myBarChart = new Chart(ctx).Bar({
                labels: ["Total in", "Total out", "Total in - Total out"],
                datasets: [
                    {
                        label: "My First dataset",
                        fillColor: "rgba(151,187,205,0.5)",
                        strokeColor: "rgba(151,187,205,0.8)",
                        highlightFill: "rgba(151,187,205,0.75)",
                        highlightStroke: "rgba(151,187,205,1)",
                        data: [<?= $item['in']; ?>, <?= $item['out']; ?>, <?= $item['in_out']; ?>]
                    }
                ]
            });
        </script>

        <table class="table table-striped table-hover table-responsive">
            <thead>
            <tr>
                <th>Дата</th>
                <th>TOTAL IN</th>
                <th>TOTAL OUT</th>
                <th>TOTAL IN - TOTAL OUT</th>
            </tr>
            </thead>
            <tbody>
                <tr>
                    <td><?= $formatter->format($item['date'], 'date'); ?></td>
                    <td><?= Price::format($item['in']); ?></td>
                    <td><?= Price::format($item['out']); ?></td>
                    <td><?= Price::format($item['in_out']); ?></td>
                </tr>
            </tbody>
        </table>
    </div><br /><br />
<?php endforeach; ?>