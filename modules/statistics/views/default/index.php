<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use kartik\date\DatePicker;
use yii\grid\GridView;

/** @var array|null $data */
/** @var yii\web\View $this */

$this->params['breadcrumbs'][] = 'Статистика';

?>

<h2 class="page-title">Статистика</h2>

<div class="statistics-default-index block">

    <span>Выберите промежуток дат для отчета.</span>

    <?php $formWidget = ActiveForm::begin(['method' => 'get']); ?>

    <?= DatePicker::widget([
        'separator' => 'До',
        'model' => $form,
        'form' => $formWidget,
        'attribute' => 'dateFrom',
        'attribute2' => 'dateTo',
        'type' => DatePicker::TYPE_RANGE,
        'pluginOptions' => [
            'format' => 'yyyy-mm-dd',
        ]
    ]); ?>

    <br />

    <?= Html::submitButton('Найти', ['class' => 'btn btn-success btn-block']); ?>
    <?php ActiveForm::end(); ?>
</div> <br /><br />

<?php if (!is_null($data) && count($data['items']) > 0): ?>
    <h2 class="page-title">Работа лототрона</h2>

    <div class="block">
        <div class="text-center">
            <canvas id="myChart" width="700" height="700"></canvas>
        </div>

        <script>
            var ctx = $('#myChart').get(0).getContext('2d');
            var myChart = new Chart(ctx).Radar({
                labels: [<?= implode(', ', $data['radarChart']['labels']); ?>],
                datasets: [
                    {
                        label: "Работа лототрона",
                        fillColor: "rgba(151,187,205,0.2)",
                        strokeColor: "rgba(151,187,205,1)",
                        pointColor: "rgba(151,187,205,1)",
                        pointStrokeColor: "#fff",
                        pointHighlightFill: "#fff",
                        pointHighlightStroke: "rgba(151,187,205,1)",
                        data: [<?= implode(',', $data['radarChart']['values']); ?>]
                    },
                    {
                        label: "Среднее число",
                        fillColor: "rgba(9, 255, 0, 0)",
                        strokeColor: "rgba(9, 255, 0, 1)",
                        pointColor: "rgba(9, 255, 0, 1)",
                        pointStrokeColor: "#fff",
                        pointHighlightFill: "#fff",
                        pointHighlightStroke: "rgba(9, 255, 0, 1)",
                        data: [<?= implode(',', $data['radarChart']['averageValues']); ?>]
                    }
                ]
            });
        </script>
    </div><br><br>

    <h2 class="page-title">Статистика по выпаданиям</h2>

    <div class="block">
        <table class="table table-striped table-hover">
            <thead>
                <tr>
                    <th>Число</th>
                    <th>Количество выпаданий</th>
                    <th>Процент выпаданий</th>
                </tr>
            </thead>
            <tbody>
                <?php foreach ($data['table'] as $tableItem): ?>
                    <tr>
                        <td><?= $tableItem['number']; ?></td>
                        <td><?= $tableItem['thisCount']; ?></td>
                        <td><?= number_format($tableItem['percent'], 2); ?>%</td>
                    </tr>
                <?php endforeach; ?>
            </tbody>
        </table>
    </div>

<?php endif; ?>
