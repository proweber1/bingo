<?php

namespace app\modules\statistics\controllers;

use Yii;
use app\modules\statistics\controllers\actions\ReportAction;
use app\controllers\system\SecurityController;

/**
 * Class DefaultController
 * @package app\modules\statistics\controllers
 */
class DefaultController extends SecurityController
{
    /**
     * @inheritdoc
     */
    public function actions()
    {
        return [
            'index' => [
                'class'   => ReportAction::className(),
                'service' => Yii::$container->get('app\services\lototron\LototronLogService'),
            ]
        ];
    }
}
