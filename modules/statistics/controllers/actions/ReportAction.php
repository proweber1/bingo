<?php

namespace app\modules\statistics\controllers\actions;

use app\components\ArrayHelper;
use Yii;
use app\modules\statistics\reports\ReportServiceInterface;
use yii\base\Action;

class ReportAction extends Action
{
    /**
     * @var ReportServiceInterface
     */
    public $service;
    /**
     * @var string
     */
    public $method = 'get';
    /**
     * @var string
     */
    public $view = 'index';
    /**
     * @var string
     */
    public $scenario = 'default';
    /**
     * @var array
     */
    public $params = [];

    /**
     * @inheritdoc
     */
    public function run()
    {
        /** @var \app\modules\statistics\forms\BallDateRangeForm $form */
        $form = Yii::$container->get('app\modules\statistics\forms\BallDateRangeForm');
        $form->scenario = $this->scenario;

        if ($form->load(Yii::$app->request->get()) && $form->validate()) {
            $reportInterface = $this->service->getReportObject($form);
            $report = $reportInterface->report();
        }

        return $this->controller->render($this->view, ArrayHelper::merge([
            'form' => $form,
            'data' => isset($report) ? $report : null
        ], $this->params));
    }
}