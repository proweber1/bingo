<?php

namespace app\modules\statistics\controllers;

use Yii;
use app\controllers\system\SecurityController;
use app\modules\statistics\controllers\actions\ReportAction;

/**
 * Class MoneyController
 * @package app\modules\statistics\controllers
 */
class MoneyController extends SecurityController
{
    /**
     * @inheritdoc
     */
    public function actions()
    {
        return [
            'index' => [
                'class' => ReportAction::className(),
                'service' => Yii::$container->get('app\services\tickets\TicketHistoryReportService'),
            ],
        ];
    }
}