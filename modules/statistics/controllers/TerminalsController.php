<?php

namespace app\modules\statistics\controllers;

use app\modules\statistics\forms\BallDateRangeForm;
use app\modules\tickets\models\tickets_history\Bingo37TicketsHistory;
use app\modules\statistics\controllers\actions\ReportAction;
use app\modules\tickets\controllers\system\SecurityController;

/**
 * Class TerminalsController
 * @package app\modules\statistics\controllers
 */
class TerminalsController extends SecurityController
{
    /**
     * @inheritdoc
     */
    public function actions()
    {
        return [
            'index' => [
                'class' => ReportAction::className(),
                'service' => \Yii::$container->get('app\services\terminals\TerminalReport'),
                'view' => 'terminal_report',
                'scenario' => BallDateRangeForm::TERMINALS_STATISTIC,
                'params' => [
                    'actionTypes' => (new Bingo37TicketsHistory())->historyActions(),
                ],
            ]
        ];
    }
}