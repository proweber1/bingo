<?php

namespace app\modules\rest\controllers;

use Yii;
use yii\rest\ActiveController;
use yii\web\NotFoundHttpException;

/**
 * Class TicketController
 * @package app\modules\tickets\controllers
 */
class TicketsController extends ActiveController
{
    /**
     * @var string
     */
    public $modelClass = 'app\modules\tickets\models\tickets\Bingo37Tickets';

    /**
     * @param $barcode
     * @return mixed
     * @throws NotFoundHttpException
     * @throws \yii\base\InvalidConfigException
     */
    public function actionBarcode($barcode)
    {
        /** @var \app\services\tickets\TicketService $service */
        $service = Yii::$container->get('app\services\tickets\TicketService');
        $model = $service->getModelByBarcode($barcode);

        if (!$model) {
            throw new NotFoundHttpException('Такой тикет не найден');
        }

        return ['id' => $model->id];
    }
}