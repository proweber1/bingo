<?php

namespace app\modules\users\models\login_log;

/**
 * This is the ActiveQuery class for [[Bingo37LoginLog]].
 *
 * @see Bingo37LoginLog
 */
class Bingo37LoginLogQuery extends \yii\db\ActiveQuery
{
    /*public function active()
    {
        $this->andWhere('[[status]]=1');
        return $this;
    }*/

    /**
     * @inheritdoc
     * @return Bingo37LoginLog[]|array
     */
    public function all($db = null)
    {
        return parent::all($db);
    }

    /**
     * @inheritdoc
     * @return Bingo37LoginLog|array|null
     */
    public function one($db = null)
    {
        return parent::one($db);
    }
}