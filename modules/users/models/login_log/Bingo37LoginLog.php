<?php

namespace app\modules\users\models\login_log;

use Yii;
use app\interfaces\DbStorageItem;
use app\interfaces\UserLogger;
use app\models\users\User;
use yii\behaviors\TimestampBehavior;

/**
 * This is the model class for table "{{%login_log}}".
 *
 * @property integer $id
 * @property integer $user_id
 * @property integer $action
 * @property integer $created_at
 * @property integer $updated_at
 *
 * @property User $user
 */
class Bingo37LoginLog extends \yii\db\ActiveRecord implements DbStorageItem
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%user_log}}';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['user_id', 'action'], 'required'],
            [['user_id', 'action', 'created_at', 'updated_at'], 'integer']
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'id',
            'user_id' => 'Пользователь',
            'actionText' => 'Действие',
            'created_at' => 'Дата',
            'updated_at' => 'Дата изменения',
            'user.fullName' => 'Действие совершил',
        ];
    }

    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'timestamp' => [
                'class' => TimestampBehavior::className(),
                'createdAtAttribute' => 'created_at',
                'updatedAtAttribute' => 'updated_at',
            ]
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getUser()
    {
        return $this->hasOne(User::className(), ['id' => 'user_id']);
    }

    /**
     * @inheritdoc
     * @return Bingo37LoginLogQuery the active query used by this AR class.
     */
    public static function find()
    {
        return new Bingo37LoginLogQuery(get_called_class());
    }

    /**
     * @inheritdoc
     */
    public function getId()
    {
        return $this->id;
    }

    private function actionAliases()
    {
        return [
            UserLogger::ACT_LOGIN => 'Вход в систему',
            UserLogger::ACT_LOGOUT => 'Выход из системы',
            UserLogger::ACT_SETTINGS_UPDATE => 'Изменение настроек',
        ];
    }

    /**
     * Преобразовываем флаг action в человеку
     * понятный текст
     *
     * @return string
     */
    public function getActionText()
    {
        $al = $this->actionAliases();

        if (isset($al[$this->action])) {
            return $al[$this->action];
        }

        throw new \InvalidArgumentException('Нет такого alias для действия');
    }
}
