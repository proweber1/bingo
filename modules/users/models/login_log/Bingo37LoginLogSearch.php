<?php

namespace app\modules\users\models\login_log;

use Yii;
use app\models\users\User;
use yii\base\Model;
use yii\data\ActiveDataProvider;

/**
 * Bingo37LoginLogSearch represents the model behind the search form about `app\modules\users\models\login_log\Bingo37LoginLog`.
 */
class Bingo37LoginLogSearch extends Bingo37LoginLog
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id', 'user_id', 'action', 'created_at', 'updated_at'], 'integer'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = Bingo37LoginLog::find()->joinWith(['user']);

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $dataProvider->setSort([
           'attributes' => [
               'created_at' => ['default' => SORT_DESC],
               'actionText' => [
                   'asc' => ['action' => SORT_ASC],
                   'desc' => ['action' => SORT_DESC],
                   'default' => SORT_ASC,
               ],
               'user.fullName' => [
                   'asc' => [User::tableName().'.fullName' => SORT_ASC],
                   'desc' => [User::tableName().'.fullName' => SORT_DESC],
                   'default' => SORT_ASC,
               ]
           ]
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        $query->andFilterWhere([
            'id' => $this->id,
            'user_id' => $this->user_id,
            'action' => $this->action,
            'created_at' => $this->created_at,
            'updated_at' => $this->updated_at,
        ]);

        return $dataProvider;
    }
}
