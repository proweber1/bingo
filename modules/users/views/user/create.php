<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\users\User */

$this->title = 'Create User';
$this->params['breadcrumbs'][] = ['label' => 'Users', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>

<h2><?= Html::encode($this->title) ?></h2>

<div class="user-create block">
    <?= $this->render('_form', ['model' => $model]); ?>
</div>
