<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\users\User */

$this->title = 'Изменение пользователя: ' . ' ' . Html::encode($model->fullName);
$this->params['breadcrumbs'][] = ['label' => 'Пользователи', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => Html::encode($model->fullName), 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = 'Изменение';
?>

<h2><?= Html::encode($this->title) ?></h2>

<div class="user-update block">
    <?= $this->render('_form', ['model' => $model]); ?>
</div>
