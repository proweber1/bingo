<?php

use yii\helpers\Html;
use yii\widgets\DetailView;
use rmrevin\yii\fontawesome\FA;

/* @var $this yii\web\View */
/* @var $model app\models\users\User */

$this->title = Html::encode($model->fullName);
$this->params['breadcrumbs'][] = ['label' => 'Пользователи', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>

<h2><?= Html::encode($model->fullName) ?></h2>

<div class="user-view block">

    <p>
        <?= Html::a('Изменить', ['update', 'id' => $model->id], ['class' => 'btn btn-primary']) ?>

        <?php if ($model->id !== Yii::$app->getUser()->getId()) : ?>
            <?= Html::a('Удалить', ['Удалить', 'id' => $model->id], [
                'class' => 'btn btn-danger',
                'data' => [
                    'confirm' => 'Вы действительно хотите удалить этого пользователя?',
                    'method' => 'post',
                ],
            ]) ?>
        <?php endif; ?>

    </p>

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'login',
            'fullName',
            [
                'attribute' => 'photo',
                'value' => $model->getThumbFileUrl('photo', 'thumb', Yii::getAlias('@web').'/images/noavatar.png'),
                'format' => 'image',
            ]
        ],
    ]) ?>

</div>
