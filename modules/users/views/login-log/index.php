<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel app\modules\users\models\login_log\Bingo37LoginLogSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Пользовательские логи';
$this->params['breadcrumbs'][] = $this->title;
?>

<h2 class="page-title"><?= Html::encode($this->title) ?></h2>

<div class="bingo37-login-log-index block">

    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            [
                'attribute' => 'user.fullName',
                'value' => function($model) {
                    return Html::a(Html::encode($model->user->fullName), ['/users/user/view', 'id' => $model->user->id]);
                },
                'format' => 'html',
            ],
            'actionText',
            'created_at:datetime',

//            ['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>

</div>
