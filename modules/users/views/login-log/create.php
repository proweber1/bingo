<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model app\modules\users\models\login_log\Bingo37LoginLog */

$this->title = 'Create Bingo37 Login Log';
$this->params['breadcrumbs'][] = ['label' => 'Bingo37 Login Logs', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="bingo37-login-log-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
