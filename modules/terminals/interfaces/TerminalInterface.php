<?php

namespace app\modules\terminals\interfaces;

/**
 * Interface TerminalInterface
 * @package app\modules\terminals\interfaces
 */
interface TerminalInterface
{
    /**
     * @return mixed
     */
    public function getId();

    /**
     * @return mixed
     */
    public function getName();

    /**
     * @return mixed
     */
    public function getStatus();

    /**
     * @return mixed
     */
    public function getStatusText();
}