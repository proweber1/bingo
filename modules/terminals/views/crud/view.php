<?php

use yii\helpers\Html;
use yii\widgets\DetailView;
use rmrevin\yii\fontawesome\FA;

/* @var $this yii\web\View */
/* @var $model app\modules\terminals\models\terminals\Bingo37Terminals */

$this->title = $model->name;
$this->params['breadcrumbs'][] = ['label' => 'Список терминалов', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>

<h2><?= Html::encode($this->title) ?></h2>

<div class="bingo37-terminals-view block">

    <p>
        <?= Html::a('Изменить', ['update', 'id' => $model->id], ['class' => 'btn btn-primary']) ?>
        <?= Html::a('Удалить', ['delete', 'id' => $model->id], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => 'Are you sure you want to delete this item?',
                'method' => 'post',
            ],
        ]) ?>

        <?= Html::a(FA::icon('refresh').'&nbsp;Перезагрузить', '#', [
            'class' => 'btn btn-warning refresh-terminal',
            'data' => [
                'terminal' => $model->id,
            ],
        ]); ?>

        <?= Html::a(FA::icon('lock').'&nbsp;Заблокировать', '#', [
            'class' => 'btn btn-warning lock-terminal',
            'data' => [
                'terminal' => $model->id,
            ],
        ]); ?>

        <?= Html::a(FA::icon('unlock').'&nbsp;Разблокировать', '#', [
            'class' => 'btn btn-success unlock-terminal',
            'data' => [
                'terminal' => $model->id,
            ],
        ]); ?>
    </p>

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'name',
            'statusText',
            'ticketId',
            'created:datetime',
            'updated:datetime',
        ],
    ]) ?>

</div>
