<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\modules\terminals\models\terminals\Bingo37Terminals */

$this->title = 'Изменение терминала: ' . ' ' . Html::encode($model->name);
$this->params['breadcrumbs'][] = ['label' => 'Список терминалов', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => Html::encode($model->name), 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = 'Изменение';
?>

<h2><?= Html::encode($this->title) ?></h2>

<div class="bingo37-terminals-update block">

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
