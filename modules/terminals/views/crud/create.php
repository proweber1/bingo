<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model app\modules\terminals\models\terminals\Bingo37Terminals */

$this->title = 'Добавление терминала';
$this->params['breadcrumbs'][] = ['label' => 'Список терминалов', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>

<h2><?= Html::encode($this->title) ?></h2>

<div class="bingo37-terminals-create block">

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
