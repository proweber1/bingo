<?php

use yii\helpers\Html;
use yii\grid\GridView;
use rmrevin\yii\fontawesome\FA;

/* @var $this yii\web\View */
/* @var $searchModel app\modules\terminals\models\terminals\Bingo37TerminalsSerarch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Список терминалов';
$this->params['breadcrumbs'][] = $this->title;
?>

<h2><?= Html::encode($this->title) ?></h2>

<div class="bingo37-terminals-index block">

    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <p>
        <?= Html::a('Добавить терминал', ['create'], ['class' => 'btn btn-success']) ?>
        <?= Html::a('Перезагрузить все терминалы', '#', ['class' => 'btn btn-danger refresh-terminals']) ?>
        <?= Html::a('Заблокировать все терминалы', '#', ['class' => 'btn btn-warning lock-terminals']) ?>
    </p>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            'name',
            [
                'attribute' => 'active',
                'value' => function ($model) {
                    /** @var \app\modules\terminals\models\terminals\Bingo37Terminals */
                    return $model->statusText;
                },
            ],
            'ticketId',
            'created:datetime',
            'updated:datetime',

            ['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>

</div>