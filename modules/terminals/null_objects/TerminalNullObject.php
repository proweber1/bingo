<?php

namespace app\modules\terminals\null_objects;

use app\modules\terminals\interfaces\TerminalInterface;

/**
 * Class TerminalNullObject
 *
 * This design pattern NullObject, NullObject is
 * used for the terminal not to check for null in
 * the service history of a ticket
 *
 * @see https://github.com/domnikl/DesignPatternsPHP/tree/master/Behavioral/NullObject
 * @package app\modules\terminals\null_objects
 */
class TerminalNullObject implements TerminalInterface
{
    /**
     * @return int
     */
    public function getId()
    {
        return 0;
    }

    public function getName()
    {

    }

    public function getStatus()
    {

    }

    public function getStatusText()
    {

    }
}