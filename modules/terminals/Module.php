<?php

namespace app\modules\terminals;

class Module extends \yii\base\Module
{
    public $controllerNamespace = 'app\modules\terminals\controllers';

    public function init()
    {
        parent::init();

        // custom initialization code goes here
    }
}
