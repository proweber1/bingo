<?php

namespace app\modules\terminals\models\terminals;

use app\modules\terminals\interfaces\TerminalInterface;
use app\modules\tickets\models\tickets\Bingo37Tickets;
use Yii;
use yii\behaviors\TimestampBehavior;
use yii\db\Expression;

/**
 * This is the model class for table "{{%terminals}}".
 *
 * @property integer $id
 * @property string $name
 * @property integer $active
 * @property integer $ticketId
 * @property integer $created
 * @property integer $updated
 */
class Bingo37Terminals extends \yii\db\ActiveRecord implements TerminalInterface
{
    public function behaviors()
    {
        return [
            'timestamp' => [
                'class' => TimestampBehavior::className(),
                'updatedAtAttribute' => 'updated',
                'createdAtAttribute' => 'created',
                'value' => new Expression('NOW()'),
            ]
        ];
    }

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%terminals}}';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['name'], 'required'],
            [['active', 'ticketId', 'created', 'updated'], 'integer'],
            [['name'], 'string', 'max' => 150],
            [['name'], 'unique'],
            [['ticketId'], 'unique'],
            [['ticketId', 'active', 'created', 'updated'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'name' => 'Название',
            'active' => 'Статус',
            'ticketId' => 'Текущий тикет',
            'created' => 'Создано',
            'updated' => 'Обновлено',
        ];
    }

    /**
     * @inheritdoc
     * @return Bingo37TerminalsQuery the active query used by this AR class.
     */
    public static function find()
    {
        return new Bingo37TerminalsQuery(get_called_class());
    }

    /**
     * @return bool
     */
    public function isExistsTicket()
    {
        return (bool) $this->getTicket();
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getTicket()
    {
        return $this->hasOne(Bingo37Tickets::className(), ['id' => 'ticketId']);
    }

    /**
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * @return int
     */
    public function getStatus()
    {
        return $this->active;
    }

    /**
     * @return string
     */
    public function getStatusText()
    {
        return $this->getStatus() ? 'Активен' : 'Неактивен';
    }
}
