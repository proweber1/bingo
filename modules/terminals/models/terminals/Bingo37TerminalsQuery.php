<?php

namespace app\modules\terminals\models\terminals;

/**
 * This is the ActiveQuery class for [[Bingo37Terminals]].
 *
 * @see Bingo37Terminals
 */
class Bingo37TerminalsQuery extends \yii\db\ActiveQuery
{
    /*public function active()
    {
        $this->andWhere('[[status]]=1');
        return $this;
    }*/

    /**
     * @inheritdoc
     * @return Bingo37Terminals[]|array
     */
    public function all($db = null)
    {
        return parent::all($db);
    }

    /**
     * @inheritdoc
     * @return Bingo37Terminals|array|null
     */
    public function one($db = null)
    {
        return parent::one($db);
    }
}