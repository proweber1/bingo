<?php

namespace app\modules\terminals\models\terminals;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\modules\terminals\models\terminals\Bingo37Terminals;

/**
 * Bingo37TerminalsSerarch represents the model behind the search form about `app\modules\terminals\models\terminals\Bingo37Terminals`.
 */
class Bingo37TerminalsSerarch extends Bingo37Terminals
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id', 'active', 'ticketId', 'created', 'updated'], 'integer'],
            [['name'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = Bingo37Terminals::find();

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        $query->andFilterWhere([
            'id' => $this->id,
            'active' => $this->active,
            'ticketId' => $this->ticketId,
            'created' => $this->created,
            'updated' => $this->updated,
        ]);

        $query->andFilterWhere(['like', 'name', $this->name]);

        return $dataProvider;
    }
}
