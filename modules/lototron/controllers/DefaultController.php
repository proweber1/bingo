<?php

namespace app\modules\lototron\controllers;

use app\modules\tickets\controllers\system\SecurityController;

class DefaultController extends SecurityController
{
    public function actionIndex()
    {
        return $this->render('index');
    }
}
