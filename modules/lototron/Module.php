<?php

namespace app\modules\lototron;

class Module extends \yii\base\Module
{
    public $controllerNamespace = 'app\modules\lototron\controllers';

    public function init()
    {
        parent::init();

        // custom initialization code goes here
    }
}
