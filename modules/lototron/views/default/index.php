<?php

/** @var yii\web\View $this */
$this->title = 'Ручной ввод';
$this->params['breadcrumbs'][] = $this->title;

?>

<style>
    div#buttons > div {
        margin-top: 7px;
        margin-bottom: 7px;
    }
</style>

<h2 class="page-title"><?= $this->title; ?></h2>

<div class="lototron-default-index block">
    <div id="buttons" class="row"></div>
</div>

<script>
    function Buttons() {}

    Buttons.prototype.render = function (selectorContainer) {
        var container = $(selectorContainer);

        for (var i = 0; i < 37; i++) {
            var code = '<div class="col-sm-2 text-center">' +
                '<a href="#" class="btn btn-success disabled number-button">' + i + '</a>' +
                '</div>';
            container.append(code);
        }
    };

    Buttons.prototype.eventClick = function (link) {
        this.disableAll();
    };

    Buttons.prototype.disableAll = function () {
        $('.number-button').addClass('disabled');
    };

    $(function () {
        var buttons = new Buttons();
        buttons.render('#buttons');

        $('a.number-button').click(function() {
            buttons.eventClick($(this));
        });
    });
</script>
