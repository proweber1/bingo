<?php

/** @var yii\web\View $this */
use yii\widgets\ActiveForm;
use yii\helpers\Html;
use yii\bootstrap\Nav;

$this->title = 'Настройки ставок';
$this->params['breadcrumbs'][] = $this->title;

?>

<h2 class="page-title"><?= $this->title; ?></h2>

<div class="block">
    <?= $this->render('/navbar'); ?>

    <?php $form = ActiveForm::begin(['id' => 'site-settings-form']); ?>

    <?= $form->field($model, 'onlyStraight')->checkbox(); ?>

    <?= Html::submitButton('Сохранить', ['class' => 'btn btn-primary text-uppercase btn-block']); ?>

    <?php ActiveForm::end(); ?>

</div>