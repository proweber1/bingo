<?php

/** @var yii\web\View $this */
use yii\bootstrap\Nav;
use yii\helpers\Html;

?>

<?= Nav::widget([
    'items' => [
        ['label' => 'Билет', 'url' => ['/settings/ticket/index']],
        ['label' => 'Ставки', 'url' => ['/settings/rates/index']],
//        ['label' => 'Управляющий модуль', 'url' => ['/settings/control-module/index']]
    ],
    'options' => ['class' => 'nav nav-tabs bg-slate nav-justified'],
]); ?>

<?php if (Yii::$app->session->hasFlash('success')) : ?>
    <?php foreach (Yii::$app->session->getFlash('success') as $message) : ?>
        <div class="alert alert-success"><?= Html::encode($message); ?></div>
    <?php endforeach; ?>
<?php endif; ?>