<?php

use yii\widgets\ActiveForm;
use yii\helpers\Html;

/** @var yii\web\View $this */
/** @var app\modules\settings\models\forms\ControlModule $model */

$this->title = 'Настройки управляющего модуля';
$this->params['breadcrumbs'][] = $this->title;

?>

<h2 class="page-title"><?= $this->title; ?></h2>

<div class="block">
    <?= $this->render('/navbar'); ?>

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'mixingTime'); ?>

    <?= $form->field($model, 'moreMixingTime'); ?>

    <?= $form->field($model, 'waitingTime'); ?>

    <?= $form->field($model, 'timeoutOnScanning'); ?>

    <?= $form->field($model, 'showTimeBall'); ?>

    <?= $form->field($model, 'withoutBalanceMode')->radioList($model->getWithoutBalanceRadioList(), [
        'separator' => '<br>',
    ]); ?>

    <?= $form->field($model, 'errorReadingMode')->radioList($model->getErrorReadingModeRadioList(), [
        'separator' => '<br>',
    ]); ?>

    <?= Html::submitButton('Сохранить', ['class' => 'btn btn-primary text-uppercase btn-block']); ?>

    <?php ActiveForm::end(); ?>
</div>
