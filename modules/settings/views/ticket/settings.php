<?php

/** @var yii\web\View $this */
use yii\widgets\ActiveForm;
use yii\helpers\Html;
use yii\bootstrap\Nav;

$this->title = 'Настройки билета';
$this->params['breadcrumbs'][] = $this->title;

?>

<h2 class="page-title"><?= $this->title; ?></h2>

<div class="block">
    <?= $this->render('/navbar'); ?>

    <?php $form = ActiveForm::begin(['id' => 'site-settings-form']); ?>

    <?= $form->field($model, 'casinoName'); ?>

    <?= $form->field($model, 'phone')->input('text', [
        'data-mask' => '0 (000) 000 - 0000',
        'placeholder' => '8 (000) 000 - 0000',
    ]); ?>

    <?= $form->field($model, 'site'); ?>

    <?= $form->field($model, 'address'); ?>

    <?= $form->field($model, 'casinoInformation'); ?>

    <?= $form->field($model, 'ticketInformation'); ?>

    <?= Html::submitButton('Сохранить', ['class' => 'btn btn-primary text-uppercase btn-block']); ?>

    <?php ActiveForm::end(); ?>

</div>