<?php

namespace app\modules\settings\controllers;

use app\modules\settings\controllers\base\SettingsController;

/**
 * Class TicketController
 * @package app\modules\settings\controllers
 */
class TicketController extends SettingsController
{
    /**
     * @var string
     */
    public $modelClass = 'app\modules\settings\models\forms\Ticket';
}