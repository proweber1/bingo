<?php

namespace app\modules\settings\controllers;

use app\modules\settings\controllers\base\SettingsController;

/**
 * Class RatesController
 * @package app\modules\settings\controllers
 */
class RatesController extends SettingsController
{
    /**
     * @var string
     */
    public $modelClass = 'app\modules\settings\models\forms\Rates';
}