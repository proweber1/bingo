<?php

namespace app\modules\settings\controllers\base;

use app\components\actions\SettingsAction;
use app\controllers\system\SecurityController;
use app\models\users\User;

/**
 * Class SettingsController
 * @package app\modules\settings\controllers\base
 */
class SettingsController extends SecurityController
{
    /**
     * @return array
     */
    public function actions()
    {
        return [
            'index' => [
                'class' => SettingsAction::className(),
                'modelClass' => $this->modelClass,
                'viewName' => 'settings',
            ]
        ];
    }

    /**
     * @return array
     */
    public function accessRules()
    {
        return [
            ['allow' => true, 'roles' => [User::SUPERADMIN_ROLE]],
        ];
    }
}