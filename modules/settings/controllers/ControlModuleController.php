<?php

namespace app\modules\settings\controllers;

use app\modules\settings\controllers\base\SettingsController;

/**
 * Class ControlModuleController
 * @package app\modules\settings\controllers
 */
class ControlModuleController extends SettingsController
{
    /**
     * @var string
     */
    public $modelClass = 'app\modules\settings\models\forms\ControlModule';
}