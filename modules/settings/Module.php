<?php

namespace app\modules\settings;

/**
 * Class Module
 * @package app\modules\settings
 */
class Module extends \pheme\settings\Module
{
    /**
     * @var string
     */
    public $controllerNamespace = 'app\modules\settings\controllers';

    /**
     * @var string
     */
    public $defaultRoute = 'ticket/index';

    public function init()
    {
        parent::init();

        // custom initialization code goes here
    }
}
