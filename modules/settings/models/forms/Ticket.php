<?php

namespace app\modules\settings\models\forms;

use yii\base\Model;
use yii\validators;

/**
 * Class Ticket
 * @package app\modules\settings\models\forms
 */
class Ticket extends Model
{
    /**
     * @var string
     */
    public $casinoName;
    /**
     * @var string
     */
    public $phone;
    /**
     * @var string
     */
    public $site;
    /**
     * @var string
     */
    public $address;
    /**
     * @var string
     */
    public $casinoInformation;
    /**
     * @var string
     */
    public $ticketInformation;

    public function rules()
    {
        return [
            [['casinoName', 'phone', 'address'], 'required'],
            [['casinoName', 'phone', 'address', 'site', 'casinoInformation', 'ticketInformation'], 'string'],
        ];
    }

    /**
     * @return array
     */
    public function attributeLabels()
    {
        return [
            'casinoName' => 'Название казино',
            'phone' => 'Номер телефона',
            'site' => 'Сайт',
            'address' => 'Адрес',
            'casinoInformation' => 'Информация о казино',
            'ticketInformation' => 'Информация о билете',
        ];
    }
}