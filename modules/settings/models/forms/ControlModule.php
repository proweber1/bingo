<?php

namespace app\modules\settings\models\forms;

use yii\base\Model;

class ControlModule extends Model
{
    /**
     * @var int Mixing time
     */
    public $mixingTime;
    /**
     * @var int More mixing time
     */
    public $moreMixingTime;
    /**
     * @var int Waiting time
     */
    public $waitingTime;
    /**
     * @var int Timeout on scanning
     */
    public $timeoutOnScanning;
    /**
     * @var int Show ball time limit
     */
    public $showTimeBall;
    /**
     * @var int Terminal mode when there is no balance
     */
    public $withoutBalanceMode;
    /**
     * @var int Behavior when unable to read the code off the ball
     */
    public $errorReadingMode;

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['mixingTime', 'moreMixingTime', 'waitingTime', 'timeoutOnScanning', 'showTimeBall', 'withoutBalanceMode', 'errorReadingMode'], 'integer']
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'mixingTime' => 'Время перемешивания шаров (от сигнала "делайте ваши ставки" до сигнала "ставок больше нет")',
            'moreMixingTime' => 'Время дополнительного мешания (от сигнала "Ставок больше нет", до отключения мешания)',
            'waitingTime' => 'Время от отключения мешания до выдвижения трубки',
            'timeoutOnScanning' => 'Таймаут на считываение шарика',
            'showTimeBall' => 'Время показа выпавшего шара',
            'withoutBalanceMode' => 'Что делать управляющему модулю, при отсутсвие баланса на терминалах',
            'errorReadingMode' => 'Что делать в случае ошибки считывания',
        ];
    }

    /**
     * Returns a list of radio buttons for the case when
     * the terminals have no money
     *
     * @return array
     */
    public function getWithoutBalanceRadioList()
    {
        return [
            1 => 'Демо-режим мешания шаров',
            2 => 'Обычная игра',
        ];
    }

    /**
     * Returns a list of radio buttons for when the Lotto was
     * not able to identify the ball
     *
     * @return array
     */
    public function getErrorReadingModeRadioList()
    {
        return [
            1 => 'Ручной ввод',
            2 => 'Сигнал - ошибочная игра'
        ];
    }
}