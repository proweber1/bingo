<?php

namespace app\modules\settings\models\forms;

use yii\base\Model;

/**
 * Class Rates
 * @package app\modules\settings\models\forms
 */
class Rates extends Model
{
    /**
     * @var boolean
     */
    public $onlyStraight;

    /**
     * @return array
     */
    public function rules()
    {
        return [
            ['onlyStraight', 'safe'],
        ];
    }

    /**
     * @return array
     */
    public function attributeLabels()
    {
        return [
            'onlyStraight' => 'Только straight',
        ];
    }
}