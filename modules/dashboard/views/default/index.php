<?php

/** @var yii\web\View $this */
/** @var $terminals */
/** @var \app\modules\terminals\models\terminals\Bingo37Terminals */

use yii\helpers\Html;

$this->params['breadcrumbs'][] = 'Dashboard';

?>

<h2>Список терминалов</h2>

<div class="row">
    <?php foreach ($terminals as $terminal) : ?>
        <div class="col-md-4">
            <div class="block" id="terminal_<?= $terminal->id; ?>">

                <div class="row">
                    <div class="col-md-9">
                        <h3 style="margin: 0;"><?= Html::encode($terminal->name); ?></h3>
                    </div>
                    <div class="col-md-3">
                        <?php if ($terminal->active) : ?>
                            <span class="terminal-status bg-success"></span>
                        <?php else : ?>
                            <span class="terminal-status bg-danger"></span>
                        <?php endif; ?>
                    </div>
                </div>

                <br />

                <div>
                    <p>
                        Текущий билет:
                        <span class="current-ticket" style="display: inline-block">
                            <?= $terminal->ticket ? Html::a($terminal->ticket->barcode, [
                                '/tickets/tickets/view', 'id' => $terminal->ticket->id
                            ]) : 'Терминал свободен'; ?>
                        </span>
                    </p>
                    <p>
                        Текущий баланс:
                        <span class="current-balance" style="display: inline-block">
                            <?= $terminal->ticket ? $terminal->ticket->totalAmount : 0; ?>
                        </span>
                    </p>
                </div>

            </div>
        </div>
    <?php endforeach; ?>
</div>