<?php

namespace app\modules\dashboard\controllers;

use Yii;
use app\controllers\system\SecurityController;

/**
 * Class DefaultController
 * @package app\modules\dashboard\controllers
 */
class DefaultController extends SecurityController
{
    /**
     * @return string
     */
    public function actionIndex()
    {
        /** @var \app\services\terminals\TerminalService $respository */
        $repository = Yii::$container->get('app\services\terminals\TerminalService');

        return $this->render('index', [
            'terminals' => $repository->getTerminals(),
        ]);
    }
}
