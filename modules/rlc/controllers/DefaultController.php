<?php

namespace app\modules\rlc\controllers;

use Yii;
use app\modules\rlc\forms\ActivateForm;
use yii\web\Controller;

/**
 * Class DefaultController
 * @package app\modules\rlc\controllers
 */
class DefaultController extends Controller
{
    /**
     * Activate action
     *
     * @return string
     */
    public function actionIndex()
    {
        $activateForm = new ActivateForm();

        if ($activateForm->load(Yii::$app->getRequest()->post()) && $activateForm->validate()) {
            if ($activateForm->activate()) {
                $url = !Yii::$app->getUser()->isGuest ? ['/'] : ['/auth/login'];
                $this->redirect($url);
            } else {
                Yii::$app->getSession()->setFlash('error', 'Не удалось активировать систему');
            }
        }

        return $this->render('index', [
            'form' => $activateForm
        ]);
    }
}
