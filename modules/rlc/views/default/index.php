<?php

use yii\widgets\ActiveForm;
use yii\helpers\Html;

/** @var yii\web\View $this */
$this->title = 'Активация';

$this->params['breadcrumbs'][] = $this->title;

?>

<div class="row" style="margin-top: 5%;">
    <div class="col-md-4 col-md-offset-4">
        <div class="panel panel-default">
            <div class="panel-heading">
                <h3>Активация системы</h3>
            </div>
            <?php $wdg = ActiveForm::begin([
                'options' => [
                    'class' => 'form-horizontal',
                ]
            ]); ?>
                <div class="panel-body">
                    <?= $wdg->field($form, 'activateKey', [
                        'labelOptions' => ['class' => 'col-sm-2 control-label'],
                        'template' => "{label}\n<div class='col-sm-10'>{input}\n{hint}\n{error}</div>",
                    ]); ?>
                </div>
                <div class="panel-footer">
                    <div class="form-group">
                        <div class="col-sm-10 col-sm-offset-2">
                            <?= Html::submitButton('Активировать', ['class' => 'btn btn-success']); ?>
                        </div>
                    </div>
                </div>
            <?php ActiveForm::end(); ?>
        </div>
    </div>
</div>