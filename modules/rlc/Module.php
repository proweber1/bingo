<?php

namespace app\modules\rlc;

class Module extends \yii\base\Module
{
    public $controllerNamespace = 'app\modules\rlc\controllers';
    public $layout = '/auth';

    public function init()
    {
        parent::init();

        // custom initialization code goes here
    }
}
