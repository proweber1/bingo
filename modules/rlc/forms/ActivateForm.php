<?php

namespace app\modules\rlc\forms;

use Yii;
use app\rlc\RlcFactory;
use yii\base\Model;

class ActivateForm extends Model
{
    /**
     * @var string license key
     */
    public $activateKey;

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            ['activateKey', 'required'],
            ['activateKey', 'string'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'activateKey' => 'Ключ активации',
        ];
    }

    /**
     * Activate licence
     *
     * @return bool|static
     */
    public function activate()
    {
        $rlsFactory = new RlcFactory();
        $rls = $rlsFactory->createRlc();

        return $rls->activate($this->activateKey);
    }
}