<?php

namespace app\rlc;

/**
 * Interface RlcFactoryInterface
 * @package app\rlc
 */
interface RlcFactoryInterface
{
    /**
     * Create rls class
     *
     * @return RlcInterface
     */
    public function createRlc();
}