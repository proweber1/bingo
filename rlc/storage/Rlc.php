<?php

namespace app\rlc\storage;

use app\rlc\RlcArInterface;
use Yii;

/**
 * This is the model class for table "{{%rlc}}".
 *
 * @property integer $id
 * @property string $key
 */
class Rlc extends \yii\db\ActiveRecord implements RlcArInterface
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%rlc}}';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['key'], 'required'],
            [['key'], 'string', 'max' => 255],
            [['key'], 'unique']
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'key' => 'Key',
        ];
    }

    /**
     * @inheritdoc
     * @return RlcQuery the active query used by this AR class.
     */
    public static function find()
    {
        return new RlcQuery(get_called_class());
    }

    /**
     * @inheritdoc
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @inheritdoc
     */
    public function getKey()
    {
        return $this->key;
    }

    /**
     * @inheritdoc
     */
    public function setKey($key)
    {
        $this->key = $key;
    }
}
