<?php

namespace app\rlc\storage;

/**
 * This is the ActiveQuery class for [[Rlc]].
 *
 * @see Rlc
 */
class RlcQuery extends \yii\db\ActiveQuery
{
    /*public function active()
    {
        $this->andWhere('[[status]]=1');
        return $this;
    }*/

    /**
     * @inheritdoc
     * @return Rlc[]|array
     */
    public function all($db = null)
    {
        return parent::all($db);
    }

    /**
     * @inheritdoc
     * @return Rlc|array|null
     */
    public function one($db = null)
    {
        return parent::one($db);
    }
}