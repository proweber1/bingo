<?php

namespace app\rlc\storage;

use app\activeRecordServices\abstraction\classes\Service as BaseService;
use app\rlc\RlcDataException;
use app\rlc\RlcServiceInterface;

/**
 * RlcService
 *
 * @method Rlc getModel()
 */
class RlcService extends BaseService implements RlcServiceInterface
{
    /**
     * RlcService constructor
     *
     * @param Rlc $model
     */
    public function __construct(Rlc $model)
    {
        $this->model = $model;
    }

    /**
     * @inheritdoc
     */
    public function getRlc()
    {
        $data = $this->getModel()->find()->orderBy(['id' => SORT_DESC])->one();

        if (!$data) {
            throw new RlcDataException('Rlc not found!');
        }

        return $data;
    }

    /**
     * @inheritdoc
     */
    public function createRlc($key)
    {
        $model = $this->getModel();
        $model->key = $key;
        return $model->save();
    }
}
