<?php

namespace app\rlc;

/**
 * Interface RlcArInterface
 * @package app\rlc
 */
interface RlcArInterface
{
    /**
     * Return record id
     *
     * @return int
     */
    public function getId();

    /**
     * Return rlc key
     *
     * @return string
     */
    public function getKey();

    /**
     * Set rlc key
     *
     * @param string $key
     * @return void
     */
    public function setKey($key);
}