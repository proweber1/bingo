<?php

namespace app\rlc;

use Yii;
use linslin\yii2\curl\Curl;

/**
 * Class RlcFactory
 *
 * ```php
 * $rlcFactory = new RlcFactory();
 * $rlc = $rlcFactory->createRlc();
 * ```
 *
 * @package app\rlc
 */
class RlcFactory implements RlcFactoryInterface
{
    /**
     * @return RlcServiceInterface
     */
    public function getRlcAr()
    {
        return Yii::$container->get('app\rlc\storage\RlcService');
    }

    /**
     * @inheritdoc
     */
    public function createRlc()
    {
        return new RlcClass($this->getRlcAr(), new Curl());
    }
}