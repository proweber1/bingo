<?php

namespace app\rlc;

use linslin\yii2\curl\Curl;

/**
 * Class RlcClass
 * @package app\rlc
 */
class RlcClass implements RlcInterface
{
    /**
     * @var RlcServiceInterface
     */
    private $rlcAr;

    /**
     * @var Curl
     */
    private $curl;

    /**
     * @var string
     */
    private $rlcServer = 'http://rlc.bingo37.loc/v1';

    /**
     * @return string
     */
    public function getRlcServer()
    {
        return $this->rlcServer;
    }

    /**
     * @return RlcServiceInterface
     */
    protected function getRlcAr()
    {
        return $this->rlcAr;
    }

    /**
     * @return Curl
     */
    protected function getCurl()
    {
        return $this->curl;
    }

    /**
     * @inheritdoc
     */
    public function __construct(RlcServiceInterface $rlcAr, Curl $curl)
    {
        $this->rlcAr = $rlcAr;
        $this->curl = $curl;
    }

    /**
     * @inheritdoc
     */
    public function checkRls()
    {
        $url = $this->getRlcServer().'?';
        $url .= http_build_query([
            'license_key' => urlencode($this->getRlcAr()->getRlc()->getKey())
        ]);

        $res = $this->getCurl()->get($url, false);

        return $res['status'] === 'ok';
    }

    /**
     * @inheritdoc
     */
    public function getRlsDays()
    {
    }

    /**
     * @inheritdoc
     */
    public function activate($key)
    {
        $res = $this->getCurl()
            ->setOption(CURLOPT_POSTFIELDS, http_build_query([
                'license_key' => $key,
            ]))->post($this->getRlcServer(), false);

        if ($res['status'] === 'ok') {
            return $this->getRlcAr()->createRlc($key);
        }

        return false;
    }
}