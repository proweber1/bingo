<?php

namespace app\rlc;

/**
 * Interface RlcServiceInterface
 * @package app\rlc
 */
interface RlcServiceInterface
{
    /**
     * Return rlc object data
     *
     * @return RlcArInterface
     */
    public function getRlc();

    /**
     * Add rlc key
     *
     * @param string $key
     * @return static
     */
    public function createRlc($key);
}