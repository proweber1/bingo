<?php

namespace app\rlc;

use linslin\yii2\curl\Curl;

/**
 * Interface RlcInterface
 * @package app\rlc
 */
interface RlcInterface
{
    /**
     * RlcInterface constructor.
     * @param RlcServiceInterface $rlcAr
     * @param Curl $curl
     */
    public function __construct(RlcServiceInterface $rlcAr, Curl $curl);

    /**
     * Check rls enabled
     *
     * @return bool
     */
    public function checkRls();

    /**
     * Return rls days
     *
     * @return integer
     */
    public function getRlsDays();

    /**
     * Activate system
     *
     * @return bool
     */
    public function activate($key);
}