'use strict';

window.paceOptions = {
    elements: false,
    ajax: false,
    document: true,
    restartOnRequestAfter: false
};

function submitTicketForm (e) {
    e.preventDefault();

    var barcode = $(this).find('#barcodeNumber').val();

    $.get('/rest/tickets/barcode', { barcode: barcode })
        .done(function (r) {
            location.href = '/tickets/tickets/view?id=' + r.id
        })
        .fail(function (r) {
            swal({
                title: 'Ошибка',
                type: 'error',
                text: r.responseJSON.message
            });
        });
}

$(function () {
    $('.print').click(function () {
        $('#ticket').printArea();
    });

    $('#ticket-payment').on('submit', submitTicketForm);
    $('.without-button').on('click', function (e) {
        $(this).addClass('disabled');
    });
});