'use strict';

function addTicketLink(ticket_block, data)
{
    var link = '<a href="/tickets/tickets/view/?id=' + data.id + '">' +
        data.barcode + '</a>';

    ticket_block.find('.current-ticket').html(link);
    ticket_block.find('.current-balance').html(data.balance.balance);
}

function addEmptyText(ticket_block)
{
    ticket_block.html('Терминал свободен');
}

$(function () {

    var socket = io('http://localhost:3000/');

    socket.emit('choose room', { room: 'admins' });

    /**
     * In this event, we get the status of the terminal,
     * there are only two, active and inactive, probably
     * better this event will be divided into two equal
     * events, one of which will be to notify that the terminal
     * is switched off, and the second that included
     */
    socket.on('terminal_status', function (data) {
        console.log(data);

        var terminal = $('#terminal_' + data.terminalId).find('.terminal-status');

        if (data.status === 1) {
            terminal.attr('class', 'terminal-status bg-success');
        } else {
            terminal.attr('class', 'terminal-status bg-danger');
        }
    });

    /**
     * In the event I receive information about what the
     * ticket is now playing at the terminal
     */
    socket.on('terminal ticket busy', function (data) {
        console.log(data);

        var ticket_block  = $('#terminal_' + data.terminal_id);

        addTicketLink(ticket_block, data.ticket);
    });

    /**
     * In this event, I have been informed that the terminal was
     * free and available for new tickets
     */
    socket.on('terminal_ticket_free', function (data) {
        console.log(data);

        var ticket_block  = $('#terminal_' + data.terminalId).find('.current-ticket');

        addEmptyText(ticket_block);
    });

    socket.emit('money_push', { ticket_barcode: 1452868256003 });

    /**
     * Restart all terminals connected to the system
     */
    $('.refresh-terminals').click(function () {
        console.log('Send event on terminals reload');

        socket.emit('terminals reload');
    });

    $('.refresh-terminal').click(function () {
        console.log('Terminal refreshed!');

        var terminal_id = $(this).data('terminal');

        console.log(terminal_id);

        socket.emit('terminal refresh', {
            id: terminal_id
        })
    });

    $('.lock-terminals').click(function () {
        console.log('Lock all terminals');

        socket.emit('lock terminals');
    });

    $('.lock-terminal').click(function () {
        console.log('Lock single terminal');

        var terminal_id = $(this).data('terminal');
        socket.emit('lock terminal', {
            id: terminal_id
        });
    });

    $('.unlock-terminal').click(function () {
        console.log('Unlock terminal');

        var terminal_id = $(this).data('terminal');
        socket.emit('unlock terminal', {
            id: terminal_id
        })
    });
});