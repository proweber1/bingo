#!/usr/bin/env bash
echo -e "PRESS CTRL + C to exit\n"

touch config/db.php

read -p "Enter db host (default: localhost): " server
server=${server:-localhost}

read -p "Enter db user (default: root): " username
username=${username:-root}

read -p "Enter db password (default: root): " password
password=${password:-root}

read -p "Enter db name (default: bingo): " dbName
dbName=${dbName:-bingo}

printf "\n"

cat << DbSettings
***************************************************
Host: $server
Username: $username
Password: $password
Db name: $dbName
***************************************************
DbSettings

dbFile="<?php \n

\rreturn [\n
    \t'class' => 'yii\db\Connection',\n
    \t'dsn' => 'mysql:host=$server;dbname=$dbName',\n
    \t'username' => '$username',\n
    \t'password' => '$password',\n
    \t'charset' => 'utf8',\n
    \t'tablePrefix' => 'bingo37_',
\r];"

rm config/db.php
echo -n -e $dbFile >> config/db.php

printf "\n"

echo -e "CREATE DB FILE: ... OK\n"
echo -e "INSTALL ASSETS FIX PLUGIN\n"
composer global require "fxp/composer-asset-plugin:~1.2.0"

echo -e "\nCOMPOSER INSTALL\n"
composer install

INTERACTIVE_FLAG=0

echo -e "LOCAL MIGRATIONS: \n"
php yii migrate --interactive=$INTERACTIVE_FLAG

echo -e "\nRBAC MIGRATIONS: \n"
php yii migrate --migrationPath=@yii/rbac/migrations --interactive=$INTERACTIVE_FLAG

echo -e "\nSETTINGS MODULE MIGRATIONS: \n"
php yii migrate --migrationPath=@vendor/pheme/yii2-settings/migrations --interactive=$INTERACTIVE_FLAG

echo -e "\nGENERATE RBAC: ... OK\n"
php yii rbac/init

cat << StartUp
********************************************************
For a quick start-up, follow these instructions

1. cd ./web
2. php -S localhost:8000

Click on the url in the browser: http://localhost:8000

DEFAULT LOGIN DATA:

username: admin
password: XWuiYdlj
********************************************************
StartUp