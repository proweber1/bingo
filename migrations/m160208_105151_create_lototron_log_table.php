<?php

use yii\db\Schema;
use yii\db\Migration;

class m160208_105151_create_lototron_log_table extends Migration
{
    private $tableName = '{{%lototron_log}}';

    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
        $this->createTable($this->tableName, [
            'id'        => $this->primaryKey(),
            'number'    => $this->integer()->notNull(),
            'createdAt' => $this->date(),
            'updatedAt' => $this->date(),
        ]);
    }

    public function safeDown()
    {
        $this->dropTable($this->tableName);
    }
}
