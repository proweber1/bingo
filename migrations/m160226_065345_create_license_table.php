<?php

use yii\db\Migration;

class m160226_065345_create_license_table extends Migration
{
    /** @var string */
    private $tblName = '{{%rlc}}';

    public function up()
    {
        $this->createTable($this->tblName, [
            'id'  => $this->primaryKey(),
            'key' => $this->string()->unique(),
        ]);
    }

    public function down()
    {
        $this->dropTable($this->tblName);
    }
}
