<?php

use yii\db\Schema;
use yii\db\Migration;

class m160115_102233_create_terminals_table extends Migration
{
    private $table = '{{%terminals}}';

    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
        $this->createTable($this->table, [
            'id' => $this->primaryKey(),
            'name' => $this->string(150)->notNull()->unique(),
            'active' => $this->boolean()->defaultValue(1), // default terminal active
            'ticketId' => $this->integer()->unique(),
            'created' => $this->dateTime(),
            'updated' => $this->dateTime(),
        ]);

        $this->insert($this->table, [
            'name' => 'Terminal #1',
            'created' => time(),
            'updated' => time(),
        ]);
    }

    public function safeDown()
    {
        $this->dropTable($this->table);
    }
}
