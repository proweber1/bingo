<?php

use yii\db\Migration;

class m160302_074530_add_is_new_game_flag extends Migration
{
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
        $this->addColumn('{{%games}}', 'isNew', $this->boolean()->defaultValue(1));
    }

    public function safeDown()
    {
        $this->dropColumn('{{%games}}', 'isNew');
    }
}
