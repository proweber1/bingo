<?php

use yii\db\Schema;
use yii\db\Migration;

class m160118_080122_add_ticket_active_temistampt_field extends Migration
{
    protected $ticketTbl = '{{%tickets}}';

    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
        $this->addColumn($this->ticketTbl, 'activated', $this->dateTime());
    }

    public function safeDown()
    {
        $this->dropColumn($this->ticketTbl, 'activated');
    }
}
