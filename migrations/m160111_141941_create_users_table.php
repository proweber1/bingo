<?php

use yii\db\Schema;
use yii\db\Migration;

class m160111_141941_create_users_table extends Migration
{
    protected $table = '{{%users}}';

    /**
     * @return string
     */
    public function getTable()
    {
        return $this->table;
    }

    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
        $this->createTable($this->getTable(), [
            'id' => $this->primaryKey(),
            'login' => $this->string(20)->notNull()->unique(),
            'password' => $this->string(156)->notNull(),
            'fullName' => $this->string(250)->notNull(),
            'photo' => $this->string(156),
        ]);
    }

    public function safeDown()
    {
        $this->dropTable($this->getTable());
    }
}
