<?php

use yii\db\Schema;
use yii\db\Migration;

class m160113_124454_create_tickets_table extends Migration
{
    /**
     * @var string
     */
    private $table = '{{%tickets}}';
    /**
     * @var string
     */
    private $users = '{{%users}}';
    /**
     * @var string
     */
    private $ticketsHistory = '{{%tickets_histories}}';
    /**
     * @var string
     */
    private $ticketsActions = '{{%tickets_actions}}';

    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
        $this->createTable($this->table, [
            'id' => $this->primaryKey(),
            'cashierId' => $this->integer()->notNull(),
            'barcode' => $this->bigInteger()->notNull()->unique(),
            'created' => $this->dateTime(),
            'updated' => $this->dateTime(),
            'status' => $this->boolean()->defaultValue(0),
        ]);

        $this->addForeignKey('cashier', $this->table, 'cashierId', $this->users, 'id', 'CASCADE');

        $this->createTable($this->ticketsActions, [
            'id' => $this->primaryKey(),
            'name' => $this->string(150),
            'alias' => $this->string(150),
        ]);

        $this->createTable($this->ticketsHistory, [
            'id' => $this->primaryKey(),
            'ticketId' => $this->integer()->notNull(),
            'terminalId' => $this->integer()->notNull(),
            'actionType' => $this->integer()->notNull(),
            'amount' => $this->decimal(10, 2)->notNull(),
            'created' => $this->dateTime(),
            'updated' => $this->dateTime(),
        ]);

        $this->addForeignKey('ticketId', $this->ticketsHistory, 'ticketId', $this->table, 'id', 'CASCADE');
        $this->addForeignKey('actionType', $this->ticketsHistory, 'actionType', $this->ticketsActions, 'id', 'CASCADE');

        $this->getDb()->createCommand()->batchInsert($this->ticketsActions, ['name', 'alias'], [
            ['depositing', 'Пополнение'],
            ['loss', 'Проигрыш'],
            ['win', 'Выигрыш'],
            ['conclusion', 'Вывод']
        ])->execute();
    }

    public function safeDown()
    {
        $this->dropForeignKey('ticketId', $this->ticketsHistory);
        $this->dropForeignKey('actionType', $this->ticketsHistory);

        $this->dropTable($this->table);
        $this->dropTable($this->ticketsActions);
        $this->dropTable($this->ticketsHistory);
    }
}
