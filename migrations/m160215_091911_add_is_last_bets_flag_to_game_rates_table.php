<?php

use yii\db\Schema;
use yii\db\Migration;

class m160215_091911_add_is_last_bets_flag_to_game_rates_table extends Migration
{
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
        $this->addColumn('{{%game_rates}}', 'isLastBets', $this->boolean()->defaultValue(1)); // Default: last bet
    }

    public function safeDown()
    {
        $this->dropColumn('{{%game_rates}}', 'isLastBets');
    }
}
