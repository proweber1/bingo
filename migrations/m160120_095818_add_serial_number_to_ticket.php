<?php

use yii\db\Schema;
use yii\db\Migration;

class m160120_095818_add_serial_number_to_ticket extends Migration
{
    protected $table = '{{%tickets}}';

    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
        $this->addColumn($this->table, 'serial_number', $this->string(11)->unique()->notNull());
    }

    public function safeDown()
    {
        $this->dropColumn($this->table, 'serial_number');
    }
}
