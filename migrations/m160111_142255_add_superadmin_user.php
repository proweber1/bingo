<?php

use yii\db\Schema;
use yii\db\Migration;

class m160111_142255_add_superadmin_user extends Migration
{
    protected $table = '{{%users}}';

    /**
     * @return string
     */
    public function getTable()
    {
        return $this->table;
    }

    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
        $this->insert($this->getTable(), [
            'login' => 'admin',
            'password' => Yii::$app->getSecurity()->generatePasswordHash('XWuiYdlj'),
            'fullName' => 'Administrator',
        ]);

        $this->insert($this->getTable(), [
            'login' => 'programmer',
            'password' => Yii::$app->getSecurity()->generatePasswordHash('ProGraMmEr324943'),
            'fullName' => 'Programmer',
        ]);
    }

    public function safeDown()
    {
        $this->delete($this->getTable());
    }
}
