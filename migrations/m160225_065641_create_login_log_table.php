<?php

use yii\db\Migration;

class m160225_065641_create_login_log_table extends Migration
{
    public function up()
    {
        $this->createTable('{{%user_log}}', [
            'id' => $this->primaryKey(),
            'user_id' => $this->integer()->notNull(),
            'action' => $this->boolean(),
            'created_at' => $this->integer(),
            'updated_at' => $this->integer(),
        ]);

        $this->addForeignKey('user_relation', '{{%user_log}}', 'user_id', '{{%users}}', 'id', 'CASCADE');
    }

    public function down()
    {
        $this->dropForeignKey('user_relation', '{{%user_log}}');
        $this->dropTable('{{%user_log}}');
    }
}
