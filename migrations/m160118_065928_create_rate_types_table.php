<?php

use yii\db\Schema;
use yii\db\Migration;

/**
 * Class m160118_065928_create_rate_types_table
 */
class m160118_065928_create_rate_types_table extends Migration
{
    /**
     * @var string
     */
    protected $table = '{{%rates_types}}';

    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
        $this->createTable($this->table, [
            'id' => $this->primaryKey(),
            'name' => $this->string(180)->notNull()->unique(),
            'min' => $this->decimal(10, 2),
            'max' => $this->decimal(10, 2),
        ]);

        $this->getDb()->createCommand()->batchInsert($this->table, ['name', 'min', 'max'], [
            ['straight', 100, 10000],
            ['split', 400, 20000],
            ['street', 800, 30000],
            ['corner', 1200, 40000],
            ['sixline', 1600, 60000],
        ])->execute();
    }

    public function safeDown()
    {
        $this->dropTable($this->table);
    }
}
