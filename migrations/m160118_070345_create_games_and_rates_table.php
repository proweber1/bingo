<?php

use yii\db\Schema;
use yii\db\Migration;

class m160118_070345_create_games_and_rates_table extends Migration
{
    /**
     * @var string
     */
    protected $games = '{{%games}}';
    /**
     * @var string
     */
    protected $tickets = '{{%tickets}}';
    /**
     * @var string
     */
    protected $gameRates = '{{%game_rates}}';
    /**
     * @var string
     */
    protected $ratesTypes = '{{%rates_types}}';

    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
        $this->createTable($this->games, [
            'id' => $this->primaryKey(),
            'ticketId' => $this->integer()->notNull(),
            'created' => $this->dateTime(),
            'updated' => $this->dateTime(),
        ]);

        $this->addForeignKey('ticket', $this->games, 'ticketId', $this->tickets, 'id', 'CASCADE');

        $this->createTable($this->gameRates, [
            'id' => $this->primaryKey(),
            'gameId' => $this->integer()->notNull(),
            'rateType' => $this->integer()->notNull(),
            'index' => $this->integer(),
            'num' => $this->integer(),
            'isNew' => $this->boolean()->defaultValue(1),
        ]);

        $this->addForeignKey('game', $this->gameRates, 'gameId', $this->games, 'id', 'CASCADE');
        $this->addForeignKey('rate', $this->gameRates, 'rateType', $this->ratesTypes, 'id', 'CASCADE');
    }

    public function safeDown()
    {
        $this->dropForeignKey('rate', $this->gameRates);
        $this->dropForeignKey('game', $this->gameRates);
        $this->dropForeignKey('ticket', $this->games);

        $this->dropTable($this->gameRates);
        $this->dropTable($this->games);
    }
}
