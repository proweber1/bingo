<?php

namespace app\controllers\system;

use Yii;
use yii\filters\AccessControl;

class SecurityController extends Controller
{
    /**
     * Of conduct for internal controllers
     *
     * @link http://www.yiiframework.com/doc-2.0/guide-rest-controllers.html
     * @return array
     */
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'rules' => $this->accessRules(),
            ]
        ];
    }

    /**
     * Here are the rules for access to certain controller actions, done so
     * for ease of use, and not to override the behavior
     *
     * @return array
     */
    protected function accessRules()
    {
        return [
            ['allow' => true, 'roles' => ['@']],
        ];
    }
}