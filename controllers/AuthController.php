<?php

namespace app\controllers;

use Yii;
use app\controllers\system\Controller;
use app\services\user\UserLoggerFactory;
use app\models\LoginLogObServer;
use app\models\LoginForm;

/**
 * Class AuthController
 * @package app\controllers
 */
class AuthController extends Controller
{
    public $layout = 'auth';

    /**
     * The user logs into the account on the basis of a
     * standard authorization
     *
     * @link https://github.com/yiisoft/yii2-app-basic/blob/master/controllers/SiteController.php#L55
     * @return string
     */
    public function actionLogin()
    {
        if (!Yii::$app->getUser()->isGuest) {
            $this->goBack();
        }

        $loginForm = new LoginForm();
        // Логирование входов
        $loginForm->attach(new LoginLogObServer());

        if ($loginForm->load(Yii::$app->getRequest()->post()) && $loginForm->login()) {
            // Redirect to dashboard
            $this->redirect(['/']);
        }

        return $this->render('login', [
            'model' => $loginForm,
        ]);
    }

    /**
     * The exit of the account
     *
     * @link https://github.com/yiisoft/yii2-app-basic/blob/master/controllers/SiteController.php#L70
     */
    public function actionLogout()
    {
        $ul = UserLoggerFactory::create();
        $ul->log($ul::ACT_LOGOUT);

        Yii::$app->getUser()->logout();

        $this->goHome();
    }
}