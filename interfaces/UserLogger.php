<?php

namespace app\interfaces;

/**
 * Interface UserLogger
 * @package app\interfaces
 */
interface UserLogger
{
    /**
     * Действие входа
     */
    const ACT_LOGIN = 1;
    /**
     * Действие выхода
     */
    const ACT_LOGOUT = 2;
    /**
     * Действие изменения настроек
     */
    const ACT_SETTINGS_UPDATE = 3;

    /**
     * Логирование действий пользователя
     *
     * @param int $action
     * @return bool
     */
    public function log($action = self::ACT_LOGIN);
}