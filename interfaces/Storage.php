<?php
/**
 * Created by PhpStorm.
 * User: vjacheslavgusser
 * Date: 12.01.16
 * Time: 18:02
 */

namespace app\interfaces;

/**
 * Interface Storage
 * @package app\interfaces
 */
interface Storage
{
    /**
     * Gets the value from the store by its
     * key
     *
     * @param $key
     * @return mixed
     */
    public function get($key);

    /**
     * This method adds a value to storage
     *
     * @param $key
     * @param $value
     * @return mixed
     */
    public function add($key, $value);

    /**
     * This method updates the value in the
     * vault
     *
     * @param $key
     * @param $value
     * @param null $newKey
     * @return mixed
     */
    public function update($key, $value, $newKey = null);

    /**
     * Remove from the storage by key
     *
     * @param $key
     * @return mixed
     */
    public function delete($key);

    /**
     * Checks for the existence of a key in the
     * keystore
     *
     * @param $key
     * @return mixed
     */
    public function has($key);
}