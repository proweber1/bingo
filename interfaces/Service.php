<?php

namespace app\interfaces;

/**
 * Interface Service
 * @package app\interfaces
 */
interface Service
{
    /**
     * Populates the model created by the service
     * data found by primary key
     *
     * @param $primaryKeyValue
     * @return mixed
     */
    public function fillModelByPrimaryKey($primaryKeyValue);

    /**
     * Returns the model created by the service
     *
     * @return \yii\db\ActiveRecord
     */
    public function getModel();
}