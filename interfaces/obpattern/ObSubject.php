<?php
/**
 * Created by PhpStorm.
 * User: vjacheslavgusser
 * Date: 14.01.16
 * Time: 12:12
 */

namespace app\interfaces\obpattern;

/**
 * Interface ObSubject
 * @package app\interfaces\obpattern
 */
interface ObSubject
{
    /**
     * @param ObServer $server
     * @return mixed
     */
    public function attach(ObServer $server);

    /**
     * @param ObServer $server
     * @return mixed
     */
    public function detach(ObServer $server);

    /**
     * @param $action
     * @param ObSubject $subject
     * @param $message
     * @return mixed
     */
    public function notify($action, ObSubject $subject, $message = null);
}