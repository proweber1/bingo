<?php
/**
 * Created by PhpStorm.
 * User: vjacheslavgusser
 * Date: 14.01.16
 * Time: 12:12
 */

namespace app\interfaces\obpattern;

/**
 * Interface ObServer
 * @package app\interfaces\obpattern
 */
interface ObServer
{
    /**
     * @param $action
     * @param ObSubject $subject
     * @param null $message
     * @return mixed
     */
    public function update($action, ObSubject $subject, $message = null);
}