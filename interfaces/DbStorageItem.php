<?php

namespace app\interfaces;

/**
 * Interface DbStorageItem
 * @package app\interfaces
 */
interface DbStorageItem
{
    /**
     * Возвращает primary key поле
     *
     * @return int
     */
    public function getId();
}