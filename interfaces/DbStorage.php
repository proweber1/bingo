<?php

namespace app\interfaces;

/**
 * Interface DbStorage
 * @package app\interfaces
 */
interface DbStorage
{
    /**
     * Вовращает итем из хранилища по
     * его id'шнику
     *
     * @param integer $id
     * @return DbStorageItem
     */
    public function getById($id);

    /**
     * Возвращает список элементов
     * DbStorageItem
     *
     * @return DbStorageItem[]
     */
    public function getAll();

    /**
     * Добавляет новый итем в хранилище
     *
     * @param array $item
     * @return bool|static
     */
    public function add(array $item);

    /**
     * Удаляет элемент из хранилища основываясь
     * на DbStorageItem
     *
     * @param DbStorageItem $item
     * @return bool
     */
    public function removeByItem(DbStorageItem $item);

    /**
     * Удаляет элемент их хранилища
     * основываясь на его id'шнике
     *
     * @param integer $id
     * @return bool
     */
    public function removeById($id);

    /**
     * Проверяет, есть ли элемент с указанным
     * id в хранилище
     *
     * @param integer $id
     * @return mixed
     */
    public function has($id);
}