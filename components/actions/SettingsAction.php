<?php

namespace app\components\actions;

use Yii;
use app\services\user\UserLoggerFactory;
use pheme\settings\Module;
use pheme\settings\SettingsAction as VendorSettingsAction;

/**
 * Class SettingsAction
 * @package app\components\actions
 */
class SettingsAction extends VendorSettingsAction
{
    /**
     * @inheritdoc
     */
    public function run()
    {
        /* @var $model \yii\db\ActiveRecord */
        $model = new $this->modelClass();
        if ($this->scenario) {
            $model->setScenario($this->scenario);
        }
        if ($model->load(Yii::$app->request->post()) && $model->validate()) {
            foreach ($model->toArray() as $key => $value) {
                Yii::$app->settings->set($key, $value, $model->formName());
            }
            Yii::$app->getSession()->addFlash('success',
                Module::t('settings', 'Successfully saved settings on {section}',
                    ['section' => $model->formName()]
                )
            );

            $ul = UserLoggerFactory::create();
            $ul->log($ul::ACT_SETTINGS_UPDATE);
        }
        foreach ($model->attributes() as $key) {
            $model->{$key} = Yii::$app->settings->get($key, $model->formName());
        }
        return $this->controller->render($this->viewName, ['model' => $model]);
    }
}