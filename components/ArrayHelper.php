<?php

namespace app\components;

use yii\helpers\ArrayHelper as BaseArrayHelper;

/**
 * Class ArrayHelper
 * @package app\components
 */
class ArrayHelper extends BaseArrayHelper
{
    /**
     * As you might guess from the name is a
     * recursive array_key_exists
     *
     * @param $needle
     * @param array $haystack
     * @return bool
     */
    public static function keyExistsR($needle, array $haystack)
    {
        $result = array_key_exists($needle, $haystack);
        if ($result) return $result;
        foreach ($haystack as $v) {
            if (is_array($v)) {
                $result = self::keyExistsR($needle, $v);
            }
            if ($result) return $result;
        }
        return $result;
    }
}