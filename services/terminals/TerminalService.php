<?php

namespace app\services\terminals;

use app\activeRecordServices\abstraction\classes\Service;
use app\modules\terminals\models\terminals\Bingo37Terminals;

/**
 * TerminalRepository
 *
 * @method Bingo37Terminals getModel()
 */
class TerminalService extends Service
{
    /**
     * TerminalRepository constructor
     *
     * @param Bingo37Terminals $model
     */
    public function __construct(Bingo37Terminals $model)
    {
        $this->model = $model;
    }

    /**
     * Returns all the terminals
     *
     * @return static[]
     */
    public function getTerminals()
    {
        return $this->getModel()->find()->all();
    }
}
