<?php

namespace app\services\terminals;

use app\activeRecordServices\abstraction\classes\Service;
use app\modules\statistics\reports\DateRangeInterface;
use app\modules\statistics\reports\ReportServiceInterface;
use app\modules\statistics\reports\TerminalsReport;
use app\modules\tickets\models\tickets_history\Bingo37TicketsHistory;
use yii\db\Expression;

/**
 * TerminalRepository
 *
 * @method Bingo37TicketsHistory getModel()
 */
class TerminalReport extends Service implements ReportServiceInterface
{
    /**
     * TerminalRepository constructor
     *
     * @param Bingo37TicketsHistory $model
     */
    public function __construct(Bingo37TicketsHistory $model)
    {
        $this->model = $model;
    }

    /**
     * @inheritdoc
     */
    public function getReportData(DateRangeInterface $form)
    {
        $query = $this->getModel()->find();
        $query->with('ticket');

        $query->select(['*', new Expression('DATE(created) as created')]);
        $query->where(['terminalId' => $form->getSelectedTerminals()]);
        $query->groupBy(['actionType', new Expression('DATE(created)')]);

        return $query->asArray()->all();
    }

    /**
     * @inheritdoc
     */
    public function getReportObject(DateRangeInterface $form)
    {
        return new TerminalsReport($this, $form);
    }
}
