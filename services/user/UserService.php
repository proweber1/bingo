<?php

namespace app\services\user;

use Yii;
use app\models\users\User;
use app\activeRecordServices\abstraction\classes\Service as BaseService;

/**
 * UserService
 */
class UserService extends BaseService
{
    /**
     * UserService constructor.
     * @param User $model
     */
    public function __construct(User $model)
    {
        $this->model = $model;
    }

    /**
     * This method creates a user and assigns a
     * standard role of the cashier
     *
     * @param array $request
     * @return bool
     */
    public function createUser(array $request)
    {
        /** @var \app\models\users\User $model */
        $model = $this->getModel();
        $model->scenario = User::SCENARIO_CREATE_USER;

        return ($model->load($request) && $model->save() && $this->addCashierRoleToUser($model));
    }

    /**
     * Adds a default role teller to user
     *
     * @param User $userModel
     * @return bool
     */
    private function addCashierRoleToUser(User $userModel)
    {
        $authManager = Yii::$app->getAuthManager();
        $authManager->assign($authManager->getRole(User::CASHIER_ROLE), $userModel->getId());

        return true;
    }

    /**
     * Update user information
     *
     * @param array $request
     * @return boolean
     */
    public function updateUser(array $request)
    {
        /** @var \app\models\users\User $model */
        $model = $this->getModel();

        return ($model->load($request) && $model->save());
    }
}
