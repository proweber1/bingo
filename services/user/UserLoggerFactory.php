<?php

namespace app\services\user;

use Yii;
use app\interfaces\DbStorage;
use app\interfaces\UserLogger as ULInterface;

/**
 * Class UserStorageFactory
 * @package app\services\user
 */
class UserLoggerFactory
{
    /**
     * Фабрика для класса логера пользователя
     *
     * @return ULInterface
     * @throws \yii\base\InvalidConfigException
     */
    public static function create()
    {
        /** @var DbStorage $service */
        $service = Yii::$container->get('app\services\user\LoginLogService');
        return new UserLogger($service);
    }
}