<?php

namespace app\services\user;

use Yii;
use app\interfaces\DbStorage;

/**
 * Class UserLogger
 * @package app\services\user
 */
class UserLogger implements \app\interfaces\UserLogger
{
    /**
     * @var DbStorage
     */
    private $storage = null;

    /**
     * UserLogger constructor.
     * @param DbStorage $storage
     */
    public function __construct(DbStorage $storage)
    {
        $this->storage = $storage;
    }

    /**
     * @inheritdoc
     */
    public function log($action = self::ACT_LOGIN)
    {
        return $this->storage->add([
            'user_id' => Yii::$app->getUser()->getId(),
            'action' => $action,
        ]);
    }
}