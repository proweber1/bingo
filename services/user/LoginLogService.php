<?php

namespace app\services\user;

use app\activeRecordServices\abstraction\classes\Service as BaseService;
use app\interfaces\DbStorage;
use app\interfaces\DbStorageItem;
use app\modules\users\models\login_log\Bingo37LoginLog;

/**
 * LoginLogService
 *
 * @method Bingo37LoginLog getModel()
 */
class LoginLogService extends BaseService implements DbStorage
{
    /**
     * LoginLogService constructor
     *
     * @param Bingo37LoginLog $model
     */
    public function __construct(Bingo37LoginLog $model)
    {
        $this->model = $model;
    }

    /**
     * @inheritdoc
     */
    public function add(array $item)
    {
        $model = $this->getModel();

        return ($model->load($item, '') && $model->save());
    }

    /**
     * @inheritdoc
     */
    public function removeById($id)
    {
        // TODO: Implement removeById() method.
    }

    /**
     * @inheritdoc
     */
    public function removeByItem(DbStorageItem $item)
    {
        // TODO: Implement removeByItem() method.
    }

    /**
     * @inheritdoc
     */
    public function getAll()
    {
        // TODO: Implement getAll() method.
    }

    /**
     * @inheritdoc
     */
    public function getById($id)
    {
        // TODO: Implement getById() method.
    }

    /**
     * @inheritdoc
     */
    public function has($id)
    {
        // TODO: Implement has() method.
    }
}
