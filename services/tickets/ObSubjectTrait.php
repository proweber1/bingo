<?php
/**
 * Created by PhpStorm.
 * User: vjacheslavgusser
 * Date: 18.01.16
 * Time: 13:33
 */

namespace app\services\tickets;

use SplObjectStorage;
use app\interfaces\obpattern\ObServer;
use app\interfaces\obpattern\ObSubject;

/**
 * Class ObsubjectTrait
 * @package app\services\tickets
 */
trait ObSubjectTrait
{
    /**
     * @var SplObjectStorage
     */
    private $observers;

    /**
     * @param ObServer $observer
     * @return mixed
     */
    public function attach(ObServer $observer)
    {
        $this->observers->attach($observer);
    }

    /**
     * @param ObServer $observer
     * @return mixed
     */
    public function detach(ObServer $observer)
    {
        $this->observers->detach($observer);
    }

    /**
     * @param $action
     * @param ObSubject $subject
     * @param null $message
     * @return mixed
     */
    public function notify($action, ObSubject $subject, $message = null)
    {
        foreach ($this->observers as $observer) {
            $observer->update($action, $subject, $message);
        }
    }
}