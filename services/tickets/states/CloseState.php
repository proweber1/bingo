<?php

namespace app\services\tickets\states;

use app\modules\tickets\models\tickets\Bingo37Tickets;
use app\services\tickets\states\abstraction\StateComposite;

/**
 * Class CloseState
 * @package app\services\tickets\states
 */
class CloseState extends StateComposite
{
    /**
     * The method sets the ticket status to closed
     *
     * @return bool
     */
    public function setStatus()
    {
        if ($this->isFinished()) {
            return false;
        }

        return $this->setFinishStatus();
    }

    /**
     * Method checks not already completed the ticket
     *
     * @return bool
     */
    private function isFinished()
    {
        return ($this->getService()->getModel()->status === Bingo37Tickets::STATUS_CLOSED);
    }

    /**
     * Method sets the model flag of closeness
     *
     * @return bool
     */
    private function setFinishStatus()
    {
        $model = $this->getService()->getModel();

        $model->status = Bingo37Tickets::STATUS_CLOSED;

        return $model->save();
    }
}