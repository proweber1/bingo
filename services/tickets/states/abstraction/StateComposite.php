<?php

namespace app\services\tickets\states\abstraction;
use app\services\tickets\TicketStatesService;

/**
 * Class StateComposite
 * @package app\services\tickets\states\abstraction
 */
abstract class StateComposite
{
    /**
     * @var TicketStatesService
     */
    private $service;

    /**
     * @return TicketStatesService
     */
    public function getService()
    {
        return $this->service;
    }

    /**
     * StateComposite constructor.
     * @param TicketStatesService $service
     */
    public function __construct(TicketStatesService $service)
    {
        $this->service = $service;
    }

    /**
     * This is an abstract function, each state class
     * needs to implement it, it sets the status of
     * the ticket through the service
     *
     * @return mixed
     */
    abstract public function setStatus();
}