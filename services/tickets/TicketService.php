<?php

namespace app\services\tickets;

use app\modules\tickets\helpers\TicketValueGenerator;
use DateTime;

use Yii;
use app\modules\tickets\helpers\Ean13Calculator;
use app\interfaces\obpattern\ObSubject;
use app\modules\tickets\models\tickets\forms\TicketCreateForm;
use app\activeRecordServices\abstraction\classes\Service as BaseService;
use app\modules\tickets\models\tickets\Bingo37Tickets;
use SplObjectStorage;
use yii\base\ExitException;

/**
 * TicketService
 *
 * @method Bingo37Tickets getModel()
 */
class TicketService extends BaseService implements ObSubject
{
    use ObSubjectTrait;

    /**
     * TicketService constructor.
     * @param Bingo37Tickets $model
     */
    public function __construct(Bingo37Tickets $model)
    {
        $this->model = $model;
        $this->observers = new SplObjectStorage();
    }

    /**
     * @param TicketCreateForm $form
     * @return bool|mixed
     * @throws \yii\base\InvalidConfigException
     */
    public function createTicket(TicketCreateForm $form)
    {
        $this->model->cashierId = Yii::$app->user->getId();
        // Ean 13
        $this->model->barcode = Ean13Calculator::generateRandomBarcode();
        // Serial Number
        $this->model->serial_number = TicketValueGenerator::generateSerialNumber();

        if ($this->model->save()) {
            $this->notify('createTicket', $this, [$this->getModel(), $form]);
            return true;
        }

        return false;
    }

    /**
     * @param $barcode
     * @return mixed
     * @throws ExitException
     */
    public function getModelByBarcode($barcode)
    {
        return $this->getModel()->find()
            ->where(['barcode' => $barcode])
            ->andWhere('status <> :finishStatus')
            ->params(['finishStatus' => Bingo37Tickets::STATUS_CLOSED])
            ->one();
    }
}
