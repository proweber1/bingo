<?php

namespace app\services\tickets\factory;

use app\modules\statistics\reports\AmountExplodeOnDaysReport;
use app\modules\statistics\reports\AmountReport;
use app\modules\statistics\reports\DateRangeInterface;
use app\modules\statistics\reports\ReportInterface;
use app\modules\statistics\reports\ReportServiceInterface;

/**
 * Class ReportFactory
 * @package app\services\tickets\factory
 */
class ReportFactory
{
    /**
     * Return ReportInterface
     *
     * @param ReportServiceInterface $service
     * @param DateRangeInterface $form
     *
     * @return ReportInterface
     */
    public static function createReportObject(ReportServiceInterface $service, DateRangeInterface $form)
    {
        if ($form->getExplodeOnDays()) {
            return new AmountExplodeOnDaysReport($service, $form);
        } else {
            return new AmountReport($service, $form);
        }
    }
}