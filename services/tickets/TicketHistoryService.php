<?php

namespace app\services\tickets;

use app\activeRecordServices\abstraction\classes\Service as BaseService;
use app\modules\terminals\interfaces\TerminalInterface;
use app\modules\tickets\models\tickets\Bingo37Tickets;
use app\modules\tickets\models\tickets_actions\Bingo37TicketsActions;
use app\modules\tickets\models\tickets_history\Bingo37TicketsHistory;

/**
 * TicketHistoryService
 *
 * @method Bingo37TicketsHistory getModel()
 */
class TicketHistoryService extends BaseService
{
    /**
     * TicketHistoryService constructor
     *
     * @param Bingo37TicketsHistory $model
     */
    public function __construct(Bingo37TicketsHistory $model)
    {
        $this->model = $model;
    }

    /**
     * Adds a log entry with a Deposit
     *
     * @param Bingo37Tickets $ticket
     * @param $amount
     * @param TerminalInterface $terminal
     * @return bool
     */
    public function addDeposit(Bingo37Tickets $ticket, $amount, TerminalInterface $terminal)
    {
        $this->fillModel(
            $ticket->id,
            $terminal->getId(),
            Bingo37TicketsActions::TYPE_DEPOSIT,
            $amount
        );

        return $this->model->save();
    }

    /**
     * @param Bingo37Tickets $ticket
     * @param TerminalInterface $terminal
     * @return bool
     */
    public function addConclusion(Bingo37Tickets $ticket, TerminalInterface $terminal)
    {
        $this->fillModel(
            $ticket->id,
            $terminal->getId(),
            Bingo37TicketsActions::TYPE_CONCLUSION,
            $ticket->totalAmount
        );

        return $this->model->save();
    }

    /**
     * @param $ticketId
     * @param $terminalId
     * @param $actionType
     * @param $amount
     */
    private function fillModel($ticketId, $terminalId, $actionType, $amount)
    {
        $this->model->ticketId = $ticketId;
        $this->model->terminalId = $terminalId;
        $this->model->actionType = $actionType;
        $this->model->amount = $amount;
    }
}
