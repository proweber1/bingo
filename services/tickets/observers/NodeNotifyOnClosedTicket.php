<?php

namespace app\services\tickets\observers;

use Yii;
use app\interfaces\obpattern\ObServer;
use app\interfaces\obpattern\ObSubject;
use app\modules\tickets\models\tickets\Bingo37Tickets;
use linslin\yii2\curl\Curl;

/**
 * Class NodeNotifyOnClosedTicket
 * @package app\services\tickets\observers
 */
class NodeNotifyOnClosedTicket implements ObServer
{
    /**
     * @param $action
     * @param ObSubject $subject
     * @param \app\modules\tickets\models\tickets\Bingo37Tickets $message
     * @return null
     */
    public function update($action, ObSubject $subject, $message = null)
    {
        if ($action === 'ticketClose' && $message->terminal) {
            $this->notifyNode($message);
        }
    }

    /**
     * Request to the NodeJS server that it can free
     * the terminal with id = ?
     *
     * @param Bingo37Tickets $model
     */
    private function notifyNode(Bingo37Tickets $model)
    {
        $nodeUrl = Yii::$app->params['nodeServer']['url'];

        $curl = new Curl();

        // Symfony style :)
        $curl
            ->setOption(CURLOPT_POSTFIELDS, http_build_query([ 'terminal_id' => $model->terminal->id ]))
            ->put($nodeUrl . '/terminals/waiting-tickets')
        ;
    }
}