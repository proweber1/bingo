<?php

namespace app\services\tickets\observers;

use app\interfaces\obpattern\ObServer;
use app\interfaces\obpattern\ObSubject;
use app\modules\terminals\null_objects\TerminalNullObject;
use app\services\tickets\observers\ticket_history_ob_server\AbstractObServer;
use app\services\tickets\TicketHistoryService;
use SplSubject;

/**
 * Class AddDepositObServer
 * @package app\services\tickets\observers
 */
class AddDepositObServer extends AbstractObServer
{
    /**
     * @param $action
     * @param ObSubject $subject
     * @param null $message
     * @return bool|null
     */
    public function update($action, ObSubject $subject, $message = null)
    {
        if ($action === 'createTicket') {
            list($ticket, $form) = $message;
            $this->service->addDeposit($ticket, $form->amount, new TerminalNullObject());
        }
    }
}