<?php

namespace app\services\tickets\observers;

use app\interfaces\obpattern\ObSubject;
use app\modules\terminals\null_objects\TerminalNullObject;
use app\services\tickets\observers\ticket_history_ob_server\AbstractObServer;

/**
 * Class ConclusionObServer
 * @package app\services\tickets\observers
 */
class ConclusionObServer extends AbstractObServer
{
    /**
     * @param $action
     * @param ObSubject $subject
     * @param null $message
     * @return null
     */
    public function update($action, ObSubject $subject, $message = null)
    {
        if ($action === 'ticketClose') {
            $this->service->addConclusion($message, new TerminalNullObject());
        }
    }
}