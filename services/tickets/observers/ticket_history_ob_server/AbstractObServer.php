<?php

namespace app\services\tickets\observers\ticket_history_ob_server;

use app\interfaces\obpattern\ObServer;
use app\interfaces\obpattern\ObSubject;
use app\services\tickets\TicketHistoryService;

/**
 * Class AbstractObServer
 * @package app\services\tickets\observers\ticket_history_ob_server
 */
abstract class AbstractObServer implements ObServer
{
    /**
     * @var TicketHistoryService
     */
    protected $service;

    /**
     * AbstractObServer constructor.
     * @param TicketHistoryService $service
     */
    public function __construct(TicketHistoryService $service)
    {
        $this->service = $service;
    }

    /**
     * @param $action
     * @param ObSubject $subject
     * @param null $message
     * @return mixed
     */
    abstract public function update($action, ObSubject $subject, $message = null);
}