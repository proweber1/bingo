<?php

namespace app\services\tickets;

use app\activeRecordServices\abstraction\classes\Service as BaseService;
use app\modules\statistics\reports\AmountReport;
use app\modules\statistics\reports\DateRangeInterface;
use app\modules\statistics\reports\ReportServiceInterface;
use app\modules\tickets\models\tickets_history\Bingo37TicketsHistory;
use app\services\tickets\factory\ReportFactory;
use yii\db\Expression;

/**
 * TicketHistoryReportService
 *
 * @method Bingo37TicketsHistory getModel()
 */
class TicketHistoryReportService extends BaseService implements ReportServiceInterface
{
    /**
     * TicketHistoryReportService constructor
     *
     * @param Bingo37TicketsHistory $model
     */
    public function __construct(Bingo37TicketsHistory $model)
    {
        $this->model = $model;
    }

    /**
     * @inheritdoc
     */
    public function getReportObject(DateRangeInterface $form)
    {
        return ReportFactory::createReportObject($this, $form);
    }

    /**
     * @inheritdoc
     */
    public function getReportData(DateRangeInterface $form)
    {
        $query = $this->getModel()->find();

        $query->select(['actionType', 'id', new Expression('SUM(amount) as amount')]);

        $query->where([
            'actionType' => [
                Bingo37TicketsHistory::ACTION_DEPOSITING,
                Bingo37TicketsHistory::ACTION_CONCLUSION
            ],
        ]);

        $query->andWhere('DATE(created) >= :dateFrom', [
            ':dateFrom' => $form->getDateFrom(),
        ]);

        if ($form->getDateTo()) {
            $query->andWhere('DATE(created) <= :dateTo', [
                ':dateTo' => $form->getDateTo(),
            ]);
        }

        $query->groupBy('actionType');

        if ($form->getExplodeOnDays()) {
            $query->addSelect(new Expression('DATE(created) as created'));
            $query->addGroupBy(new Expression('DATE(created)'));
        }

        return $query->asArray()->all();
    }
}
