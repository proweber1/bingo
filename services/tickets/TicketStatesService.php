<?php

namespace app\services\tickets;

use SplObjectStorage;
use app\services\tickets\states\CloseState;
use app\activeRecordServices\abstraction\classes\Service as BaseService;
use app\interfaces\obpattern\ObSubject;
use app\modules\tickets\models\tickets\Bingo37Tickets;

/**
 * TicketStatesService
 *
 * @method Bingo37Tickets getModel()
 */
class TicketStatesService extends BaseService implements ObSubject
{
    use ObSubjectTrait;

    /**
     * TicketStatesService constructor
     *
     * @param Bingo37Tickets $model
     */
    public function __construct(Bingo37Tickets $model)
    {
        $this->model = $model;
        $this->observers = new SplObjectStorage();
    }

    /**
     * In this method, initially we generated a new service
     * method fillModelByPrimaryKey() next, create a new CloseStatus
     * object and pass him this service, then call the method
     * setStatus() of the abstract class StateComposite and notify
     * observers that the transaction is completed
     *
     * @param $id
     * @return bool
     */
    public function finishTicket($id)
    {
        /** @var \app\services\tickets\TicketStatesService $service */
        $service = $this->fillModelByPrimaryKey($id);

        if ((new CloseState($service))->setStatus()) {
            $this->notify('ticketClose', $service, $service->getModel());

            return true;
        }

        return false;
    }
}
