<?php

namespace app\services\lototron;

use app\activeRecordServices\abstraction\classes\Service as BaseService;
use app\modules\statistics\forms\BallDateRangeForm;
use app\modules\statistics\models\lototron\Bingo37LototronLog;
use app\modules\statistics\reports\DateRangeInterface;
use app\modules\statistics\reports\ReportServiceInterface;
use app\modules\statistics\reports\StatisricReport;
use yii\data\ActiveDataProvider;
use yii\db\Expression;
use yii\helpers\ArrayHelper;

/**
 * LototronLogService
 *
 * @method Bingo37LototronLog getModel()
 */
class LototronLogService extends BaseService implements ReportServiceInterface
{
    /**
     * LototronLogService constructor
     *
     * @param Bingo37LototronLog $model
     */
    public function __construct(Bingo37LototronLog $model)
    {
        $this->model = $model;
    }

    // Make your methods

    /**
     * Returns statistics on the balls for a certain period of time
     *
     * @param DateRangeInterface $form
     * @return \app\modules\statistics\models\lototron\Bingo37LototronLog[]|array
     */
    public function getReportData(DateRangeInterface $form)
    {
        $query = $this->getModel()->find();

        $query->select(['*', new Expression('COUNT(id) as thisCount')]);
        $query->where('createdAt >= :dateFrom', [
            ':dateFrom' => $form->getDateFrom(),
        ]);

        if ($form->getDateTo()) {
            $query->andWhere('createdAt <= :dateTo', [
                ':dateTo' => $form->getDateTo(),
            ]);
        }

        $query->groupBy('number');
        $query->asArray();

        return $query->all();
    }

    /**
     * @inheritdoc
     */
    public function getReportObject(DateRangeInterface $form)
    {
        return new StatisricReport($this, $form);
    }
}
