<?php

/** @var yii\web\View $this */
/** @var app\models\LoginForm $model */
use yii\widgets\ActiveForm;
use yii\helpers\Html;
use rmrevin\yii\fontawesome\FA;

$this->params['breadcrumbs'][] = 'Авторизация';

?>

<section class="login-form block">

    <span class="login-icon center-block">
        <?= FA::icon('user-secret'); ?>
    </span>

    <div class="text-center">
        <h3>Войти в систему</h3>
        <h5>Введите свои персональные данные</h5>
    </div><br />

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'username'); ?>

    <?= $form->field($model, 'password')->passwordInput(); ?>

    <?= Html::submitButton('Войти&nbsp;&nbsp;'.FA::icon('arrow-circle-o-right'), [
        'class' => 'btn btn-primary btn-block login-button']); ?>

    <?php ActiveForm::end(); ?>
</section>
