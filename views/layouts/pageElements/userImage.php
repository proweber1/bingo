<?php use yii\helpers\Html; ?>

<?php if (!Yii::$app->getUser()->getIsGuest()) : ?>
    <?php
        $imageUrl = Yii::$app
            ->getUser()
            ->getIdentity()
            ->getThumbFileUrl('photo', 'thumb', Yii::getAlias('@web').'/images/noavatar.png');
    ?>
    <?= Html::img($imageUrl, ['class' => 'img-circle img-sm']); ?>
<?php endif; ?>