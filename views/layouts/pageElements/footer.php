<?php

/** @var yii\web\View $this */
use yii\helpers\Html;

?>

<footer>
    <div class="container">
        © <?= date('Y'); ?>. <?= Html::a('Bingo37', ['site/index']); ?>
    </div>
</footer>