<?php

/** @var yii\web\View $this */
use yii\bootstrap\NavBar;
use yii\bootstrap\Nav;
use rmrevin\yii\fontawesome\FA;
use yii\helpers\Html;

?>

<?php NavBar::begin([
    'brandLabel' => 'Bingo37',
    'options' => ['class' => 'nav navbar-inverse'],
]); ?>

<?= Nav::widget([
    'encodeLabels' => false,
    'dropDownCaret' => false,
    'items' => [
        ['label' => 'example', 'url' => '#', 'visible' => !Yii::$app->getUser()->isGuest],
        [
            'dropDownOptions' => ['class' => 'dropdown-menu-right'],
            'visible' => !Yii::$app->getUser()->isGuest,
            'label' => $this->render('userImage'),
            'items' => [
                ['label' => FA::icon('sign-out').'&nbsp;&nbsp;Выйти из профиля', 'url' => ['/auth/logout']],
            ],
        ]
    ],
    'options' => ['class' => 'nav navbar-nav pull-right top-nav'],
]); ?>

<?php NavBar::end(); ?>