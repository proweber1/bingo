<?php

/** @var yii\web\View $this */
use rmrevin\yii\fontawesome\FA;
use yii\widgets\Menu;
use yii\helpers\Html;
use app\models\users\User;

?>

<span class="block-title">
    Главное меню
</span>

<div class="block-user">
    <div class="media">
        <div class="media-left">
            <?= $this->render('userImage'); ?>
        </div>
        <div class="media-body media-middle">
            <h4 style="margin: 0;"><?= Html::encode(Yii::$app->getUser()->getIdentity()->fullName); ?></h4>
            <h5>
                <?php $roles = Yii::$app->getAuthManager()->getRolesByUser(Yii::$app->getUser()->getId()); ?>
                <?php foreach ($roles as $role) : ?>
                    <?= $role->description; ?>
                <?php endforeach; ?>
            </h5>
        </div>
    </div>
</div>

<?= Menu::widget([
    'activeCssClass' => 'active',
    'encodeLabels' => false,
    'items' => [
        [
            'label' => FA::icon('dashboard').'&nbsp;&nbsp;Dashboard',
            'url' => ['/dashboard/default/index']
        ],
        [
            'label' => FA::icon('credit-card').'&nbsp;&nbsp;Отчет по финансам',
            'url' => ['/statistics/money/index'],
        ],
        [
            'label' => FA::icon('bar-chart').'&nbsp;&nbsp;Статистика',
            'url' => ['/statistics/default/index'],
        ],
        [
            'label' => FA::icon('chain-broken').'&nbsp;&nbsp;Статистика по терминалам',
            'url' => ['/statistics/terminals/index'],
        ],
        [
            'label' => FA::icon('ticket').'&nbsp;&nbsp;Создать билет',
            'url' => ['/tickets/tickets/create'],
        ],
        [
            'label' => FA::icon('money').'&nbsp;&nbsp;Выплатить по билету',
            'url' => ['/tickets/payment/pay'],
        ],
        [
            'label' => FA::icon('th-list').'&nbsp;&nbsp;Список билетов',
            'url' => ['/tickets/tickets/index'],
        ],
        [
            'label' => FA::icon('cogs').'&nbsp;&nbsp;Настройки',
            'visible' => Yii::$app->getUser()->can(User::SUPERADMIN_ROLE),
            'active' => (Yii::$app->controller->module->id === 'settings'),
            'url' => ['/settings'],
        ],
        [
            'label' => FA::icon('user-plus').'&nbsp;&nbsp;Управление пользователями',
            'visible' => Yii::$app->getUser()->can(User::SUPERADMIN_ROLE),
            'url' => ['/users/user/index'],
        ],
        [
            'label' => FA::icon('get-pocket').'&nbsp;&nbsp;Пользовательские логи',
            'visible' => Yii::$app->getUser()->can(User::SUPERADMIN_ROLE),
            'url' => ['/users/login-log/index'],
        ],
        [
            'label' => FA::icon('terminal').'&nbsp;&nbsp;Управление терминалами',
            'visible' => Yii::$app->getUser()->can(User::SUPERADMIN_ROLE),
            'url' => ['/terminals/crud/index'],
        ],
        [
            'label' => FA::icon('star').'&nbsp;&nbsp;Управление ставками',
            'visible' => Yii::$app->getUser()->can(User::SUPERADMIN_ROLE),
            'url' => ['/rates/crud/index'],
        ],
    ],
    'options' => ['class' => 'nav nav-pills nav-stacked main-nav'],
]); ?>