<?php

/** @var yii\web\View $this */
use yii\widgets\Breadcrumbs;
use rmrevin\yii\fontawesome\FA;

?>

<?php if (array_key_exists('breadcrumbs', $this->params)) : ?>
    <section class="breadcrumbs-container">
        <div class="container">
            <?= Breadcrumbs::widget([
                'encodeLabels' => false,
                'homeLink' => [
                    'label' => FA::icon('home').'&nbsp;&nbsp;Главная',
                    'url' => ['/'],
                ],
                'links' => $this->params['breadcrumbs'],
            ]); ?>
        </div>
    </section>
<?php endif; ?>