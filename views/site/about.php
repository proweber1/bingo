<?php

/* @var $this yii\web\View */

use yii\helpers\Html;
use barcode\barcode\BarcodeGenerator;

$this->title = 'About';
$this->params['breadcrumbs'][] = $this->title;
?>

<div class="block">
    <div class="media" id="ticket">
        <div class="media-left media-middle">
            <div id="barcode" class="pull-left"></div>
            <?= BarcodeGenerator::widget([
                'elementId' => 'barcode',
                'type'      => 'ean13',
                'value'     => rand(1000000000000, 9999999999999),
                'settings'  => [
                    'barWidth' => 2,
                    'barHeight' => 80,
                ],
            ]); ?>
        </div>
        <div class="media-body text-center text-uppercase">
            <h1 class="ticket-title">Bingo 37</h1>
            <p>Лотерейный билет</p>
            <div class="row">
                <div class="col-xs-4 text-left"><h5>8 (999) 999 - 9999</h5></div>
                <div class="col-xs-4"><h5>г.Санкт-Петербург, улица Кропоткина 1</h5></div>
                <div class="col-xs-4 text-right"><h5>bingo37.su</h5></div>
            </div>

            <h1><?= number_format(9353.62, 2); ?> kzt</h1>

            <div class="row">
                <div class="col-xs-6">
                    <p class="text-justify">
                        Билет не является средством оплаты
                    </p>
                </div>
                <div class="col-xs-6 text-right">
                    <p>Администратор: Петров Сергей</p>
                    <p><?= date('d.m.Y H:i'); ?></p>
                </div>
            </div>
        </div>
        <hr />
    </div>
</div>
<br />

<button class="print btn btn-success">print</button>