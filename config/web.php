<?php

$params = require(__DIR__ . '/params.php');

$config = [
    'id' => 'basic',
    'basePath' => dirname(__DIR__),
    'bootstrap' => ['log'],
    'language' => 'ru-RU',
    'defaultRoute' => '/dashboard',
    'components' => [
        'request' => [
            // !!! insert a secret key in the following (if it is empty) - this is required by cookie validation
            'cookieValidationKey' => 'sGzWxqmBFUKUVJKFqMxpvIZi9Yy1UWJC',
        ],
        'cache' => [
            'class' => 'yii\caching\FileCache',
        ],
        'user' => [
            'identityClass' => 'app\models\users\User',
            'enableAutoLogin' => true,
            'loginUrl' => ['auth/login'],
        ],
        'errorHandler' => [
            'errorAction' => 'site/error',
        ],
        'mailer' => [
            'class' => 'yii\swiftmailer\Mailer',
            // send all mails to a file by default. You have to set
            // 'useFileTransport' to false and configure a transport
            // for the mailer to send real emails.
            'useFileTransport' => true,
        ],
        'log' => [
            'traceLevel' => YII_DEBUG ? 3 : 0,
            'targets' => [
                [
                    'class' => 'yii\log\FileTarget',
                    'levels' => ['error', 'warning'],
                ],
            ],
        ],
        'db' => require(__DIR__ . '/db.php'),
        'urlManager' => [
            'enablePrettyUrl' => true,
            'showScriptName' => false,
            'rules' => [
                [
                    'class' => 'yii\rest\UrlRule',
                    'controller' => 'rest/tickets',
                    'only' => ['options', 'view'],
                    'extraPatterns' => [
                        'GET barcode' => 'barcode',
                    ]
                ],
            ],
        ],
        'authManager' => 'yii\rbac\DbManager',
        'settings' => 'pheme\settings\components\Settings',
    ],
    'modules' => [
        'tickets' => 'app\modules\tickets\Module',
        'keys' => [
            'class' => 'pheme\settings\Module',
            'sourceLanguage' => 'ru',
            'accessRoles' => ['programmer'],
        ],
        'dashboard' => 'app\modules\dashboard\Module',
        'settings' => 'app\modules\settings\Module',
        'users' => 'app\modules\users\Module',
        'rest' => 'app\modules\rest\Module',
        'terminals' => 'app\modules\terminals\Module',
        'rates' => 'app\modules\rates\Module',
        'statistics' => 'app\modules\statistics\Module',
        'lototron' => 'app\modules\lototron\Module',
        'rlc' => 'app\modules\rlc\Module',
    ],
    'params' => $params,
];

if (YII_ENV_DEV) {
    // configuration adjustments for 'dev' environment
    $config['bootstrap'][] = 'debug';
    $config['modules']['debug'] = [
        'class' => 'yii\debug\Module',
    ];

    $config['bootstrap'][] = 'gii';
    $config['modules']['gii'] = [
        'class' => 'yii\gii\Module',
        'generators' => [
            'services' => 'app\activeRecordServices\gii\service\Generator',
        ]
    ];
}

return $config;
