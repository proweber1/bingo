<?php

return [
    'adminEmail' => 'admin@example.com',
    'nodeServer' => [
        'url' => 'http://localhost:3000/api',
    ],
];
