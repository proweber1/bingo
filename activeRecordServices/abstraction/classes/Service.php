<?php

namespace app\activeRecordServices\abstraction\classes;

use app\activeRecordServices\abstraction\interfaces\Service as ServiceInterface;

/**
 * Class Service
 * @package proweber1\activeRecordServices\abstraction\classes
 */
abstract class Service implements ServiceInterface
{
    /**
     * @var \yii\db\ActiveRecordInterface
     */
    protected $model;

    /**
     * @return mixed
     */
    public function getModel()
    {
        return $this->model;
    }

    /**
     * Populates the model created by the service
     * data found by primary key
     *
     * @param $primaryKeyValue
     * @return Service
     */
    public function fillModelByPrimaryKey($primaryKeyValue)
    {
        return new static($this->getModel()->findOne($primaryKeyValue));
    }
}