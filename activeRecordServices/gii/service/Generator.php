<?php

namespace app\activeRecordServices\gii\service;

use Yii;
use yii\db\ActiveRecord;
use yii\db\BaseActiveRecord;
use yii\gii\CodeFile;

/**
 * Class Generator
 * @package proweber1\activeRecordServices\gii\service
 */
class Generator extends \yii\gii\Generator
{
    public $modelClass;
    public $serviceClass;

    /**
     * @return array
     */
    public function rules()
    {
        return [
            [['modelClass', 'serviceClass'], 'filter', 'filter' => 'trim'],
            [['modelClass', 'serviceClass'], 'required'],
            [['modelClass', 'serviceClass'], 'string'],
            [
                ['modelClass', 'serviceClass'],
                'match',
                'pattern' => '/^[\w\\\\]*$/', 'message' => 'Only word characters and backslashes are allowed.'
            ],
            [['modelClass'], 'validateClass', 'params' => ['extends' => BaseActiveRecord::className()]],
        ];
    }

    /**
     * Checks if model class is valid
     */
    public function validateModelClass()
    {
        /* @var $class ActiveRecord */
        $class = $this->modelClass;
        $pk = $class::primaryKey();
        if (empty($pk)) {
            $this->addError('modelClass', "The table associated with $class must have primary key(s).");
        }
    }

    /**
     * @return string
     */
    public function getName()
    {
        return 'Service Generator';
    }

    /**
     * @return array
     */
    public function generate()
    {
        $serviceFile = Yii::getAlias('@' . str_replace('\\', '/', ltrim($this->serviceClass, '\\')) . '.php');

        return [
            new CodeFile($serviceFile, $this->render('service.php')),
        ];
    }
}