<?php

use yii\helpers\StringHelper;

$serviceClass = StringHelper::basename($generator->serviceClass);
$modelClass = StringHelper::basename($generator->modelClass);

echo "<?php\n";

?>

namespace <?= StringHelper::dirname($generator->serviceClass), ";\n"; ?>

use app\activeRecordServices\abstraction\classes\Service as BaseService;
use <?= $generator->modelClass; ?>;

/**
 * <?= $serviceClass, "\n"; ?>
 *
 * @method <?= StringHelper::basename($generator->modelClass) ?> getModel()
 */
class <?= $serviceClass ?> extends BaseService
{
    /**
     * <?= $serviceClass ?> constructor
     *
     * @param <?= $modelClass ?> $model
     */
    public function __construct(<?= $modelClass ?> $model)
    {
        $this->model = $model;
    }

    // Make your methods
}
